﻿function limitText(limitField, lbl, limitNum) {
    //debugger;
    if (limitField.value.length > limitNum) {
        limitField.value = limitField.value.substring(0, limitNum);

    } else {

        document.getElementById(lbl).innerHTML = limitNum - limitField.value.length;
    }
}

function limitTextAgg(limitField, limitNum) {
    //debugger;
    if (limitField.value.length > limitNum) {
        limitField.value = limitField.value.substring(0, limitNum);

    } 
}

function ValidateAadhaar(e) {

    var aadhaarNo = $(e).val().split(" ").join("");
    aadhaarNo = aadhaarNo.replace(/[^\d]/g, '');
    if (aadhaarNo.length > 0) {                
        aadhaarNo = aadhaarNo.match(new RegExp('.{1,4}', 'g')).join(" ");
    }
    $(e).val(aadhaarNo);

}

function EnableSection(ctrl) {

    if ($("#" + ctrl.id + " :selected").text() == 'Select' || $("#" + ctrl.id + " :selected").text() == '' || $("#" + ctrl.id + " :selected").text() == null) {
        ($('#txtSepcialization_4').attr("disabled", "disabled"));
        ($('#txtNameUni_4').attr("disabled", "disabled"));
        ($('#txtlocation_4').attr("disabled", "disabled"));
        ($('#dtGrad_4').attr("disabled", "disabled"));
        ($('#txtAggregate_4').attr("disabled", "disabled"));
        ($('#dtFromGrad_4').attr("disabled", "disabled"));
        ($('#dtToGrad_4').attr("disabled", "disabled"));
        ($('#SelBranch_4').attr("disabled", "disabled"));
        ($('#txtUniversity_4').attr("disabled", "disabled"));
    }
    else {
        ($('#txtSepcialization_4').val(''));
        ($('#txtSepcialization_4').removeAttr("disabled"));
        ($('#txtNameUni_4').val(''));
        ($('#txtNameUni_4').removeAttr("disabled"));
        ($('#txtlocation_4').val(''));
        ($('#txtlocation_4').removeAttr("disabled"));
        ($('#dtFromGrad_4').val(''));
        ($('#dtFromGrad_4').removeAttr("disabled"));
        ($('#dtToGrad_4').val(''));
        ($('#dtToGrad_4').removeAttr("disabled"));
        ($('#txtAggregate_4').val(''));
        ($('#txtAggregate_4').removeAttr("disabled"));
        ($('#SelBranch_4').removeAttr("disabled"));
        ($('#txtUniversity_4').removeAttr("disabled"));
        ($('#txtUniversity_4').val(''));
    }
    PopulateBranch(ctrl);
}



function convertDateFormatMMMToMM(sourceDate) {
    //debugger;
    var monthsShort = ";Jan;Feb;Mar;Apr;May;Jun;Jul;Aug;Sep;Oct;Nov;Dec";

    var array = monthsShort.split(';');
    var replaced = false;
    for (var i = 1; ((i < array.length) && (!replaced)); i++) {
        if (sourceDate.indexOf(array[i]) != -1) {
            if (i > 9)
                sourceDate = sourceDate.replace(array[i], i);
            else
                sourceDate = sourceDate.replace(array[i], "0" + i);
            return sourceDate
        }
    }
}

function convertToJSDateFormat(sourceDate) {
    var numbers = sourceDate.match(/\d+/g);
    var dt = numbers[1] + "/" + numbers[0] + "/" + numbers[2]
    var jsdt = new Date(dt);
    return jsdt;
}

function validate(SubType) {
    //  debugger;
    document.getElementById('hdnSubType').value = SubType;
    var dtToday = new Date();
    var CheckZipCode = new RegExp("^([\\d]{6})?$");
    var pattern = new RegExp("^([\\d]{10,12})?$");
    var landline = RegExp("^([\\d]{6,12})?$");
    var number = new RegExp("^[0-9]*$");
    var email = new RegExp("^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$");
    var marks = /^\d+(?:\.\d{1,20})?$/;
    var branchExp = new RegExp("^[a-zA-Z0-9]+$");
    var stateCity = new RegExp("^[a-zA-Z ]*$"); 
    var middleNameExp =new RegExp("^[a-zA-Z ]*$"); 
    
    var txtMiddleName=document.getElementById('txtMiddleName').value;
    if (txtMiddleName != null && txtMiddleName != "" && txtMiddleName != "-") {
        if (middleNameExp.test(txtMiddleName)) {
        }
        else {
            alert("Please enter valid middle name");
            ScrollValidate('divPer');
            document.getElementById('txtMiddleName').focus();
            return false;
        }
    }

    if (document.getElementById('chkFemale').checked == false && document.getElementById('chkMale').checked == false && SubType != "SAVED") {
        alert("Please select Gender");
        ScrollValidate('divPer');
        document.getElementById('chkMale').focus();
        return false;
    }
    if (document.getElementById('rdIndian').checked == false && document.getElementById('rdOthers').checked == false && SubType != "SAVED") {
        alert("Please select nationality");        
        ScrollValidate('divPer');        
        return false;
    }
    if(document.getElementById('rdOthers').checked == true && document.getElementById('ddlCountries').value=="Select"){
     alert("Please select Country");        
        ScrollValidate('divPer');   
        document.getElementById('ddlCountries').focus();     
        return false;
    }

    var aadhaar = document.getElementById('txtAadhaar').value;
    if (aadhaar != null && aadhaar != "") {
        if (aadhaar.length == 14) {
        } else {
            alert("Please enter a valid Aadhaar number");
            ScrollValidate('divPer');
            document.getElementById('txtAadhaar').focus();
            return false;
        }
    }

    var emailID = document.getElementById('lblEmail1').value;
    if (emailID == null || emailID == "" && SubType != "SAVED") {
        alert("Please enter Email ID");
        ScrollValidate('divPer');
        document.getElementById('lblEmail1').focus();
        return false;
    }
    if (email.test(emailID)) {
    } else {
        alert("Please enter a valid Email ID");
        ScrollValidate('divPer');
        document.getElementById('lblEmail1').focus();
        return false;
    }

    var mobile = document.getElementById('txtMobile').value;
    if (mobile == null || mobile == "" && SubType != "SAVED") {
        alert("Please enter mobile number");
        ScrollValidate('divPer');
        document.getElementById('txtMobile').focus();
        return false;
    }
    if (pattern.test(mobile)) {
    } else {
        alert("Please enter a valid mobile number");
        ScrollValidate('divPer');
        document.getElementById('txtMobile').focus();
        return false;
    }

    var altMobile = document.getElementById('txtAltMobile').value;
    if (altMobile == null || altMobile == "" && SubType != "SAVED") {
        alert("Please enter alternate mobile number");
        ScrollValidate('divPer');
        document.getElementById('txtAltMobile').focus();
        return false;
    }
    if (pattern.test(altMobile)) {
    } else {
        alert("Please enter a valid alternate mobile number");
        ScrollValidate('divPer');
        document.getElementById('txtAltMobile').focus();
        return false;
    }

    if(!(mobile == null || mobile == "") && !(altMobile == null || altMobile == ""))
    {
        if(mobile.trim() == altMobile.trim())
        {
            alert("The mobile number and alternate mobile number are same. Please enter different numbers.");
            ScrollValidate('divPer');
            document.getElementById('txtAltMobile').focus();
            return false;
        }
    }

    var Address = document.getElementById('txtAddress').value;
    if ((Address == null || Address == "") && SubType != "SAVED") {
        alert("Please enter address details");
        ScrollValidate('divPer');
        document.getElementById('txtAddress').focus();
        return false;
    }
//    Address = document.getElementById('txtAddress2').value;
//    if ((Address == null || Address == "") && SubType != "SAVED") {
//        alert("Please enter address details");
//        ScrollValidate('divPer');
//        document.getElementById('txtAddress2').focus();
//        return false;
//    }
    var txtState = document.getElementById('txtState').value;
    if ((txtState == null || txtState == "") && SubType != "SAVED") {
        alert("Please enter State");
        ScrollValidate('divPer');
        document.getElementById('txtState').focus();
        return false;
    }
    if (txtState != null && txtState != "") {
        if (stateCity.test(txtState)) {
        }
        else {
            alert("Please enter valid state");
            ScrollValidate('divPer');
            document.getElementById('txtState').focus();
            return false;
        }
    }
    var txtCity = document.getElementById('txtCity').value;
    if ((txtCity == null || txtCity == "") && SubType != "SAVED") {
        alert("Please enter City");
        ScrollValidate('divPer');
        document.getElementById('txtCity').focus();
        return false;
    }
    if (txtCity != null && txtCity != "") {
        if (stateCity.test(txtCity)) {
        }
        else {
            alert("Please enter valid city");
            ScrollValidate('divPer');
            document.getElementById('txtCity').focus();
            return false;
        } 
    }
    var txtPin = document.getElementById('txtPin').value;
    if ((txtPin == null || txtPin == "") && SubType != "SAVED") {
        alert("Please enter pin code");
        ScrollValidate('divPer');
        document.getElementById('txtPin').focus();
        return false;
    }
    if (CheckZipCode.test(txtPin)) {
    }
    else {
        alert("Please enter a valid pin code");
        ScrollValidate('divPer');
        document.getElementById('txtPin').focus();
        return false;
    }
    var txtLandline = document.getElementById('txtLandline').value;
//    if ((txtLandline == null || txtLandline == "") && SubType != "SAVED") {
//        alert("Please enter phone number");
//        ScrollValidate('divPer');
//        document.getElementById('txtLandline').focus();
//        return false;
//    }
    if(!(txtLandline == null || txtLandline == ""))
    {
        if (landline.test(txtLandline)) {
        }
        else {
        alert("Please enter valid phone number");
        ScrollValidate('divPer');
        document.getElementById('txtLandline').focus();
        return false;
        }
    }

    Address = document.getElementById('txtPerAddress').value;
    if ((Address == null || Address == "") && SubType != "SAVED") {
        alert("Please enter address details");
        ScrollValidate('divPer');
        document.getElementById('txtPerAddress').focus();
        return false;
    }
//    Address = document.getElementById('txtPerAddress2').value;
//    if ((Address == null || Address == "") && SubType != "SAVED") {
//        alert("Please enter address details");
//        ScrollValidate('divPer');
//        document.getElementById('txtPerAddress2').focus();
//        return false;
//    }
    txtState = document.getElementById('txtPerState').value;
    if ((txtState == null || txtState == "") && SubType != "SAVED") {
        alert("Please enter State");
        ScrollValidate('divPer');
        document.getElementById('txtPerState').focus();
        return false;
    }
    if (txtState != null && txtState != "") {
        if (stateCity.test(txtState)) {
        }
        else {
            alert("Please enter valid state");
            ScrollValidate('divPer');
            document.getElementById('txtPerState').focus();
            return false;
        }
    }

    txtCity = document.getElementById('txtPerCity').value;
    if ((txtCity == null || txtCity == "") && SubType != "SAVED") {
        alert("Please enter City");
        ScrollValidate('divPer');
        document.getElementById('txtPerCity').focus();
        return false;
    }
    if (txtCity != null && txtCity != "") {
        if (stateCity.test(txtCity)) {
        }
        else {
            alert("Please enter valid city");
            ScrollValidate('divPer');
            document.getElementById('txtPerCity').focus();
            return false;
        }
    }
    txtPin = document.getElementById('txtPerPin').value;
    if ((txtPin == null || txtPin == "") && SubType != "SAVED") {
        alert("Please enter pin code");
        ScrollValidate('divPer');
        document.getElementById('txtPerPin').focus();
        return false;
    }
    if (CheckZipCode.test(txtPin)) {
    }
    else {
        alert("Please enter a valid pin code");
        ScrollValidate('divPer');
        document.getElementById('txtPerPin').focus();
        return false;
    }
    txtLandline = document.getElementById('txtPerPhone').value;
//    if ((txtLandline == null || txtLandline == "") && SubType != "SAVED") {
//        alert("Please enter phone number");
//        ScrollValidate('divPer');
//        document.getElementById('txtPerPhone').focus();
//        return false;
//    }
    if(!(txtLandline == null || txtLandline == ""))
    {
        if (landline.test(txtLandline)) {
        }
        else {
            alert("Please enter valid phone number");
            ScrollValidate('divPer');
            document.getElementById('txtPerPhone').focus();
            return false;
        }
    }

    //         if (($('#SelCourse_1 :selected').text() || $('#SelCourse_2 :selected').text() || $('#SelCourse_3 :selected').text()|| $('#SelCourse_4 :selected').text()) == "Select") {
    //            alert("Please select course");
    //            ScrollValidate('divEdu');
    //            return false;
    //        }


    //        if (($('#SelCourse_1 :selected').text() || $('#SelCourse_2 :selected').text() || $('#SelCourse_3 :selected').text() || $('#SelCourse_4 :selected').text()) == "Select") {
    //            alert("Please select course");
    //            ScrollValidate('divEdu');
    //            return false;
    //        }


    for (var i = 1; i < 5; i++) {
        var course = $('#SelCourse_' + i + ' :selected').text();
        if (i < 3) {
            var txtSepcialization = document.getElementById('txtBranch_' + i).value;
        }
        else {
            var txtSepcialization = document.getElementById('SelBranch_' + i).value;
        }
        var txtNameUni = document.getElementById('txtNameUni_' + i).value;
        var txtlocation = document.getElementById('txtlocation_' + i).value;
        var txtAggregate = document.getElementById('txtAggregate_' + i).value;
        var x = document.getElementById('dtToGrad_' + i).value;
        var y = document.getElementById('dtFromGrad_' + i).value;
        var txtUniversity=document.getElementById('txtUniversity_' + i).value;

        if ((course == null || course == "" || course == "Select") && i < 4 && SubType != "SAVED") {
            alert("Please select course");
            ScrollValidate('divEdu');
            document.getElementById('SelCourse_' + i).focus();
            return false;
        }
        if ((course == null || course == "" || course == "Select") && i == 4 && document.getElementById('SelBranch_' + i).disabled == false && SubType != "SAVED") {
            alert("Please select course");
            ScrollValidate('divEdu');
            document.getElementById('SelCourse_' + i).focus();
            return false;
        }
        if (i >= 3) { //For 10th and 12th branch detail is not manadatory
            if ((txtSepcialization == null || txtSepcialization == "" || txtSepcialization == "Select") && document.getElementById('SelBranch_' + i).disabled == false && SubType != "SAVED") {
                alert("Please enter branch details");
                ScrollValidate('divEdu');
                document.getElementById('SelBranch_' + i).focus();
                return false;
            }
        }
        else if (txtSepcialization != "") {
            if (branchExp.test(document.getElementById('txtBranch_' + i).value)) {
            }
            else {
                alert("Please enter valid branch.");
                ScrollValidate('divPer');
                document.getElementById('txtBranch_' + i).focus();
                return false;
            }
        }        
        
        if (txtUniversity=="" && document.getElementById('txtUniversity_' + i).disabled == false && i>2 && SubType != "SAVED"){
            alert("Please enter university details");
            ScrollValidate('divEdu');
            document.getElementById('txtUniversity_' + i).focus();
            return false;
        }

        if ((txtNameUni == null || txtNameUni == "") && document.getElementById('txtNameUni_' + i).disabled == false && SubType != "SAVED") {
            alert("Please enter institution details");
            ScrollValidate('divEdu');
            document.getElementById('txtNameUni_' + i).focus();
            return false;
        }

        if ((txtlocation == null || txtlocation == "") && document.getElementById('txtlocation_' + i).disabled == false && SubType != "SAVED") {
            alert("Please enter location details");
            ScrollValidate('divEdu');
            document.getElementById('txtlocation_' + i).focus();
            return false;
        }

        if ((y == null || y == "") && document.getElementById('dtFromGrad_' + i).disabled == false && SubType != "SAVED") {
            alert("Please enter from date");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_' + i).focus();
            return false;
        }

        if ((x == null || x == "") && document.getElementById('dtToGrad_' + i).disabled == false && SubType != "SAVED") {
            alert("Please enter to date");
            ScrollValidate('divEdu');
            document.getElementById('dtToGrad_' + i).focus();
            return false;
        }

        if (SubType == "SAVED" && (x == null || x == "") && y != "") {
            alert("Please enter to date");
            ScrollValidate('divEdu');
            document.getElementById('dtToGrad_' + i).focus();
            return false;
        }

        if (SubType == "SAVED" && (y == null || y == "") && x != "") {
            alert("Please enter from date");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_' + i).focus();
            return false;
        }

        if ((txtAggregate == null || txtAggregate == "") && document.getElementById('txtAggregate_' + i).disabled == false && SubType != "SAVED") {
            alert("Please enter aggregate %");
            ScrollValidate('divEdu');
            document.getElementById('txtAggregate_' + i).focus();
            return false;
        }
        else if (document.getElementById('txtAggregate_' + i).disabled == false && txtAggregate != "") {
            if (marks.test(txtAggregate)) {
                if (eval(document.getElementById('txtAggregate_' + i).value) > 100) {
                    alert("Please enter CGPA interms of 100");
                    ScrollValidate('divEdu');
                    document.getElementById('txtAggregate_' + i).focus();
                    return false;
                }
            }
            else {
                alert("Please enter valid aggregate %");
                ScrollValidate('divEdu');
                document.getElementById('txtAggregate_' + i).focus();
                return false;
            }
        }
    }

    var loopCnt = 0;
    if (document.getElementById('SelCourse_4').value != "Select") {
        loopCnt = 4;
    }
    else {
        loopCnt = 3;
    }
    for (var i = 1; i <= loopCnt; i++) {
        var x = document.getElementById('dtToGrad_' + i).value;
        var y = document.getElementById('dtFromGrad_' + i).value;
        var month = dtToday.getMonth();
        month = month + 1;
        var year = dtToday.getFullYear();

        //To date validation
        if (x != null || x != "") {
            var array = x.split('/');
            if (array != null) {
                var m = convertDateFormatMMMToMM(array[0]);
                if(i!=loopCnt){
                    if (m > month && array[1] > year) {
                        alert("Invalid To Date");
                        ScrollValidate('divEdu');
                        document.getElementById('dtToGrad_' + i).focus();
                        return false;
                    }
                    if (m > month && array[1] == year) {
                        alert("Invalid To Date");
                        ScrollValidate('divEdu');
                        document.getElementById('dtToGrad_' + i).focus();
                        return false;
                    }
                    if (m == month && array[1] > year) {
                        alert("Invalid To Date");
                        ScrollValidate('divEdu');
                        document.getElementById('dtToGrad_' + i).focus();
                        return false;
                    }
                    if (m < month && array[1] > year) {
                        alert("Invalid To Date");
                        ScrollValidate('divEdu');
                        document.getElementById('dtToGrad_' + i).focus();
                        return false;
                    }         
                }                       
            }
        }

        //From date validation
        if (y != null || y != "") {
            var array = y.split('/');
            if (array != null) {
                var m = convertDateFormatMMMToMM(array[0]);
                if (m > month && array[1] > year) {
                    alert("Invalid From Date");
                    ScrollValidate('divEdu');
                    document.getElementById('dtFromGrad_' + i).focus();
                    return false;
                }
                if (m > month && array[1] == year) {
                    alert("Invalid From Date");
                    ScrollValidate('divEdu');
                    document.getElementById('dtFromGrad_' + i).focus();
                    return false;
                }
                if (m == month && array[1] > year) {
                    alert("Invalid From Date");
                    ScrollValidate('divEdu');
                    document.getElementById('dtFromGrad_' + i).focus();
                    return false;
                }
                if (m < month && array[1] > year) {
                    alert("Invalid From Date");
                    ScrollValidate('divEdu');
                    document.getElementById('dtFromGrad_' + i).focus();
                    return false;
                }
            }
        }
    }

    var tenthFrom = document.getElementById('dtFromGrad_1').value;
    var twelFrom = document.getElementById('dtFromGrad_2').value;
    var gradFrom = document.getElementById('dtFromGrad_3').value;
    var postFrom = document.getElementById('dtFromGrad_4').value;

    var tenthTo = document.getElementById('dtToGrad_1').value;
    var twelTo = document.getElementById('dtToGrad_2').value;
    var gradTo = document.getElementById('dtToGrad_3').value;
    var postTo = document.getElementById('dtToGrad_4').value;

    var fromArray1 = tenthFrom.split('/');
    var fromArray2 = twelFrom.split('/');
    var fromArray3 = gradFrom.split('/');
    var fromArray4 = postFrom.split('/');

    var toArray1 = tenthTo.split('/');
    var toArray2 = twelTo.split('/');
    var toArray3 = gradTo.split('/');
    var toArray4 = postTo.split('/');

    //To and From date validations

    if (fromArray1[1] != null && toArray1[1] != null) {
        if (fromArray1[1] > toArray1[1]) {
            alert("Starting year can not be greater than or equal to completion year");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_1').focus();
            return false;
        }
        else if(fromArray1[1] == toArray1[1]){
            if(parseInt(convertDateFormatMMMToMM(fromArray1[0])) >= parseInt(convertDateFormatMMMToMM(toArray1[0]))){
            alert("Starting month can not be greater than or equal to completion month");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_1').focus();
            return false;
            }
        }
    }

    if (fromArray2[1] != null && toArray2[1] != null) {
        if (fromArray2[1] > toArray2[1]) {
            alert("Starting year can not be greater than or equal to completion year");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_2').focus();
            return false;
        }
        else if(fromArray2[1] == toArray2[1]){
            if(parseInt(convertDateFormatMMMToMM(fromArray2[0])) >= parseInt(convertDateFormatMMMToMM(toArray2[0]))){
            alert("Starting month can not be greater than or equal to completion month");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_2').focus();
            return false;
            }
        }
    }

    if (fromArray3[1] != null && toArray3[1] != null) {
        if (fromArray3[1] > toArray3[1]) {
            alert("Starting year can not be greater than or equal to completion year");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_3').focus();
            return false;
        }
        else if(fromArray3[1] == toArray3[1]){
            if(parseInt(convertDateFormatMMMToMM(fromArray3[0])) >= parseInt(convertDateFormatMMMToMM(toArray3[0]))){
            alert("Starting month can not be greater than or equal to completion month");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_3').focus();
            return false;
            }
        }
    }

    if (fromArray4[1] != null && toArray4[1] != null) {
        if (fromArray4[1] > toArray4[1]) {
            alert("Starting year can not be greater than or equal to completion year");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_4').focus();
            return false;
        }
         else if(fromArray4[1] == toArray4[1]){
            if(parseInt(convertDateFormatMMMToMM(fromArray4[0])) >= parseInt(convertDateFormatMMMToMM(toArray4[0]))){
            alert("Starting month can not be greater than or equal to completion month");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_4').focus();
            return false;
            }
        }
    }


    //To date validations
    if (toArray1[1] != null && toArray2[1] != null) {
        if (toArray1[1] >= toArray2[1]) {
            alert("Completion year of 10th cannot be greater than or equal to that of 12th.");
            ScrollValidate('divEdu');
            document.getElementById('dtToGrad_1').focus();
            return false;
        }
    }

    if (toArray2[1] != null && toArray3[1] != null) {
        if (toArray2[1] >= toArray3[1]) {
            alert("Completion year of 12th cannot be greater than or equal to that of graduation");
            ScrollValidate('divEdu');
            document.getElementById('dtToGrad_2').focus();
            return false;
        }
    }
    if (toArray3[1] != null && toArray4[1] != null) {
        if (toArray3[1] >= toArray4[1]) {
            alert("Completion year of graduation cannot be greater than or equal to that of post graduation");
            ScrollValidate('divEdu');
            document.getElementById('dtToGrad_3').focus();
            return false;
        }
    }

    //From date validations

    if (fromArray1[1] != null && fromArray2[1] != null) {
        if (fromArray1[1] >= fromArray2[1]) {
            alert("Starting year of 10th cannot be greater than or equal to that of 12th.");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_1').focus();
            return false;
        }
    }

    if (fromArray2[1] != null && fromArray3[1] != null) {
        if (fromArray2[1] >= fromArray3[1]) {
            alert("Starting year for 12th cannot be greater than or equal to that of graduation");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_2').focus();
            return false;
        }
    }
    if (fromArray3[1] != null && fromArray4[1] != null) {
        if (fromArray3[1] >= fromArray4[1]) {
            alert("Starting year for graduation cannot be greater than or equal to that of post graduation");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_3').focus();
            return false;
        }
    }


    //Previous education with next level dates

    if (fromArray2[1] != null && toArray1[1] != null) {
        if (fromArray2[1] < toArray1[1] || (fromArray2[1] == toArray1[1] && convertDateFormatMMMToMM(fromArray2[0]) < convertDateFormatMMMToMM(toArray1[0]))) {
            alert("Starting date of 12th/Diploma cannot be earlier than completion date of 10th.");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_2').focus();
            return false;
        }
    }

    if (fromArray3[1] != null && toArray2[1] != null) {
        if (fromArray3[1] < toArray2[1] || (fromArray3[1] == toArray2[1] && convertDateFormatMMMToMM(fromArray3[0]) < convertDateFormatMMMToMM(toArray2[0]))) {
            alert("Starting date of graduation cannot be earlier than completion date of 12th/Diploma.");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_3').focus();
            return false;
        }
    }

    if (fromArray4[1] != null && toArray3[1] != null) {
        if (fromArray4[1] < toArray3[1] || (fromArray4[1] == toArray3[1] && convertDateFormatMMMToMM(fromArray4[0]) < convertDateFormatMMMToMM(toArray3[0]))) {
            alert("Starting date of post graduation cannot be earlier than completion date of graduation");
            ScrollValidate('divEdu');
            document.getElementById('dtFromGrad_4').focus();
            return false;
        }
    }

    if (document.getElementById('rdMathsYes').checked == false && document.getElementById('rdMathsNo').checked == false && SubType != "SAVED") {
        alert("Please select whether you have studied Maths in 12th");        
        ScrollValidate('divEdu');
        document.getElementById('rdMathsYes').focus();        
        return false;
    }

    if (document.getElementById('rdEduDecYes').checked == false && document.getElementById('rdEduDecNo').checked == false && SubType != "SAVED") {
        alert("Please select whether you have done full time degrees as per the university guidelines");        
        ScrollValidate('divEdu');
        document.getElementById('rdEduDecYes').focus();        
        return false;
    }

    //Gap
    var hdnConfiguredGap = document.getElementById('hdnGap').value;        
    document.getElementById('hdnIsGapFound_1').value="N";
    document.getElementById('hdnIsGapFound_2').value="N";
    document.getElementById('hdnIsGapFound_3').value="N";
    if (hdnConfiguredGap == "")
        hdnConfiguredGap = "0";
    if (fromArray2 != "" && toArray1 != "") {
        if(parseInt(fromArray2[1]) - parseInt(toArray1[1]) == parseInt(hdnConfiguredGap)){
            if(parseInt(convertDateFormatMMMToMM(fromArray2[0])) / parseInt(convertDateFormatMMMToMM(toArray1[0])) >= 1){
             AddGapTableRows("1","validate","");   
             document.getElementById('hdnIsGapFound_1').value="Y"; 
            } 
        }
        else if (parseInt(fromArray2[1]) - parseInt(toArray1[1]) > parseInt(hdnConfiguredGap)) {        
             AddGapTableRows("1","validate","");   
             document.getElementById('hdnIsGapFound_1').value="Y";                                                                
        }
    }

    if (fromArray3 != "" && toArray2 != "") {
     if(parseInt(fromArray3[1]) - parseInt(toArray2[1]) == parseInt(hdnConfiguredGap)){
            if(parseInt(convertDateFormatMMMToMM(fromArray3[0])) / parseInt(convertDateFormatMMMToMM(toArray2[0])) >= 1){
             AddGapTableRows("2","validate","");   
             document.getElementById('hdnIsGapFound_2').value="Y"; 
            } 
        }
        else if (parseInt(fromArray3[1]) - parseInt(toArray2[1]) > parseInt(hdnConfiguredGap)) {
               AddGapTableRows("2","validate","");
               document.getElementById('hdnIsGapFound_2').value="Y";                                                                                                       
        }
    }

    if (fromArray4 != "" && toArray3 != "") {
     if(parseInt(fromArray4[1]) - parseInt(toArray3[1]) == parseInt(hdnConfiguredGap)){
            if(parseInt(convertDateFormatMMMToMM(fromArray4[0])) / parseInt(convertDateFormatMMMToMM(toArray3[0])) >= 1){
             AddGapTableRows("3","validate","");   
             document.getElementById('hdnIsGapFound_3').value="Y"; 
            } 
        }
        else if (parseInt(fromArray4[1]) - parseInt(toArray3[1]) > parseInt(hdnConfiguredGap)) {
           AddGapTableRows("3","validate",""); 
           document.getElementById('hdnIsGapFound_3').value="Y";                                   
        }
    }
   
   var hdnGapRownum;
   if(tblgapDetails.rows.length>1){
        if(document.getElementById('txtGapDesc_1')!=undefined && document.getElementById('txtGapDesc_1')!="" && document.getElementById('txtGapDesc_1')!=null && document.getElementById('hdnIsGapFound_1').value!="Y"){
            hdnGapRownum=document.getElementById('hdnGapRownum_1').value;            
            document.getElementById("tblgapDetails").deleteRow(parseInt(hdnGapRownum));
       }
         if(document.getElementById('txtGapDesc_2')!=undefined && document.getElementById('txtGapDesc_2')!="" && document.getElementById('txtGapDesc_2')!=null && document.getElementById('hdnIsGapFound_2').value!="Y"){
            hdnGapRownum=document.getElementById('hdnGapRownum_2').value;                   
            document.getElementById("tblgapDetails").deleteRow(parseInt(hdnGapRownum));           
        }
         if(document.getElementById('txtGapDesc_3')!=undefined && document.getElementById('txtGapDesc_3')!="" && document.getElementById('txtGapDesc_3')!=null && document.getElementById('hdnIsGapFound_3').value!="Y"){
             hdnGapRownum=document.getElementById('hdnGapRownum_3').value;                     
            document.getElementById("tblgapDetails").deleteRow(parseInt(hdnGapRownum));   
        }
   }    

   if(tblgapDetails.rows.length>1 && SubType != "SAVED"){//1 is the default row. i,e column headers
       for(var tblCnt=1; eval(tblCnt)<=4; tblCnt++){
       var txtGapDesc=document.getElementById('txtGapReason'+tblCnt);
       if(txtGapDesc!="" && txtGapDesc!=null && txtGapDesc!=undefined){
           var txtGapReason=document.getElementById('txtGapReason'+tblCnt).value;
               if(txtGapReason==""){
                   alert("Please mention reason for gap between education");
                    ScrollValidate('divEdu');
                    document.getElementById('txtGapReason'+tblCnt).focus();
                    return false;
               }
           }
       }
   }
    //debugger;
    var table = document.getElementById("tblTraining");
    for (var i = 2, row; row = table.rows[i]; i++) {
        var txtTrainOrg = document.getElementById('txtTrainOrg_' + (i - 1)).value;
        if (txtTrainOrg == null || txtTrainOrg == "") {
            alert("Please enter training organisation name");
            ScrollValidate('divEdu');
            document.getElementById('txtTrainOrg_' + (i - 1)).focus();
            return false;
        }
        if(txtTrainOrg.length > 300) {
            alert("Please enter the Training Organisation value in less than 300 characters");
            ScrollValidate('divEdu');
            document.getElementById('txtTrainOrg_' + (i - 1)).focus();
            return false;
        }
        var txtSkills = document.getElementById('txtSkills_' + (i - 1)).value;
        if (txtSkills == null || txtSkills == "") {
            alert("Please enter training skills");
            ScrollValidate('divEdu');
            document.getElementById('txtSkills_' + (i - 1)).focus();
            return false;
        }
        if(txtSkills.length > 500) {
            alert("Please enter the Training Skills value in less than 500 characters");
            ScrollValidate('divEdu');
            document.getElementById('txtSkills_' + (i - 1)).focus();
            return false;
        }
        var txtTrainLocation = document.getElementById('txtTrainLocation_' + (i - 1)).value;
        if (txtTrainLocation == null || txtTrainLocation == "") {
            alert("Please enter training location");
            ScrollValidate('divEdu');
            document.getElementById('txtTrainLocation_' + (i - 1)).focus();
            return false;
        }
        if(txtTrainLocation.length > 500) {
            alert("Please enter the Training Location value in less than 500 characters");
            ScrollValidate('divEdu');
            document.getElementById('txtTrainLocation_' + (i - 1)).focus();
            return false;
        }
        var fromdate = document.getElementById('txtTrainFromDt_' + (i - 1)).value;
        if (fromdate == null || fromdate == "") {
            alert("Please enter Training start date");
            ScrollValidate('divEdu');
            document.getElementById('txtTrainFromDt_' + (i - 1)).focus();
            return false;
        }
        var todate = document.getElementById('txtTrainToDt_' + (i - 1)).value;
        if (todate == null || todate == "") {
            alert("Please enter Training end date");
            ScrollValidate('divEdu');
            document.getElementById('txtTrainToDt_' + (i - 1)).focus();
            return false;
        }

        if (fromdate != null && fromdate != "") {
            fromdate = convertDateFormatMMMToMM(fromdate);
            var dtFromDate = convertToJSDateFormat(fromdate);
            if ((Date.parse(dtToday) < Date.parse(dtFromDate))) {
                alert("Training start date cannot be greater than current date");
                ScrollValidate('divEdu');
                document.getElementById('txtTrainFromDt_' + (i - 1)).focus();
                return false;
            }
        }

         if (todate != null && todate != "") {
            todate = convertDateFormatMMMToMM(todate);
            var dtTodate = convertToJSDateFormat(todate);
            if ((Date.parse(dtToday) < Date.parse(dtTodate))) {
                alert("Training end date cannot be greater than current date");
                ScrollValidate('divEdu');
                document.getElementById('txtTrainToDt_' + (i - 1)).focus();
                return false;
            }
        }
        var fromdate = document.getElementById('txtTrainFromDt_' + (i - 1)).value;
         var todate = document.getElementById('txtTrainToDt_' + (i - 1)).value;
        if (fromdate != null && fromdate != "" && todate != null && todate != "") {
            fromdate = convertDateFormatMMMToMM(fromdate);
            var dtFromDate = convertToJSDateFormat(fromdate);
            todate = convertDateFormatMMMToMM(todate);
            var dttoDate = convertToJSDateFormat(todate);

            if ((Date.parse(dtFromDate) > Date.parse(dttoDate))) {
                alert("Training start date cannot be greater than end date");
                ScrollValidate('divEdu');
                document.getElementById('txtTrainFromDt_' + (i - 1)).focus();
                document.getElementById('txtTrainToDt_' + (i - 1)).focus();
                return false;
            }
        }     
    }

    var proj = document.getElementById("tblProject");
    for (var i = 2, row; row = proj.rows[i]; i++) {
        var txtProjName = document.getElementById('txtProjName_' + (i - 1)).value;
        if (txtProjName == null || txtProjName == "") {
            alert("Please enter Project name");
            ScrollValidate('divEdu');
            document.getElementById('txtProjName_' + (i - 1)).focus();
            return false;
        }
        if(txtProjName.length > 300) {
            alert("Please enter the Project Name value in less than 300 characters");
            ScrollValidate('divEdu');
            document.getElementById('txtProjName_' + (i - 1)).focus();
            return false;
        }
        var txtProjSkills = document.getElementById('txtProjSkills_' + (i - 1)).value;
        if (txtProjSkills == null || txtProjSkills == "") {
            alert("Please enter project skills");
            ScrollValidate('divEdu');
            document.getElementById('txtProjSkills_' + (i - 1)).focus();
            return false;
        }
        if(txtProjSkills.length > 500) {
            alert("Please enter the Project Skills value in less than 500 characters");
            ScrollValidate('divEdu');
            document.getElementById('txtProjSkills_' + (i - 1)).focus();
            return false;
        }
        var txtProjDesc = document.getElementById('txtProjDesc_' + (i - 1)).value;
        if (txtProjDesc == null || txtProjDesc == "") {
            alert("Please enter project description");
            ScrollValidate('divEdu');
            document.getElementById('txtProjDesc_' + (i - 1)).focus();
            return false;
        }
        if(txtProjDesc.length > 300) {
            alert("Please enter the Project Description value in less than 300 characters");
            ScrollValidate('divEdu');
            document.getElementById('txtProjDesc_' + (i - 1)).focus();
            return false;
        }
        var txtProjLocation = document.getElementById('txtProjLocation_' + (i - 1)).value;
        if (txtProjLocation == null || txtProjLocation == "") {
            alert("Please enter project location");
            ScrollValidate('divEdu');
            document.getElementById('txtProjLocation_' + (i - 1)).focus();
            return false;
        }
        if(txtProjLocation.length > 500) {
            alert("Please enter the Project Location value in less than 500 characters");
            ScrollValidate('divEdu');
            document.getElementById('txtProjLocation_' + (i - 1)).focus();
            return false;
        }
        var projfromdate = document.getElementById('txtProjFromDt_' + (i - 1)).value;
        if (projfromdate == null || projfromdate == "") {
            alert("Please enter Project start date");
            ScrollValidate('divEdu');
            document.getElementById('txtProjFromDt_' + (i - 1)).focus();
            return false;
        }
        var projtodate = document.getElementById('txtProjToDt_' + (i - 1)).value;
        if (projtodate == null || projtodate == "") {
            alert("Please enter Project end date");
            ScrollValidate('divEdu');
            document.getElementById('txtProjToDt_' + (i - 1)).focus();
            return false;
        }

        if (projfromdate != null && projfromdate != "") {
            projfromdate = convertDateFormatMMMToMM(projfromdate);
            var dtProjFromDate = convertToJSDateFormat(projfromdate);
            if ((Date.parse(dtToday) < Date.parse(dtProjFromDate))) {
                alert("Project start date cannot be greater than current date");
                ScrollValidate('divEdu');
                document.getElementById('txtProjFromDt_' + (i - 1)).focus();
                return false;
            }
        }

         if (projtodate != null && projtodate != "") {
            projtodate = convertDateFormatMMMToMM(projtodate);
            var dtProjTodate = convertToJSDateFormat(projtodate);
            if ((Date.parse(dtToday) < Date.parse(dtProjTodate))) {
                alert("Project end date cannot be greater than current date");
                ScrollValidate('divEdu');
                document.getElementById('txtProjToDt_' + (i - 1)).focus();
                return false;
            }
        }
        var projfromdate = document.getElementById('txtProjFromDt_' + (i - 1)).value;
         var projtodate = document.getElementById('txtProjToDt_' + (i - 1)).value;
        if (projfromdate != null && projfromdate != "" && projtodate != null && projtodate != "") {
            projfromdate = convertDateFormatMMMToMM(projfromdate);
            var dtProjFromDate = convertToJSDateFormat(projfromdate);
            projtodate = convertDateFormatMMMToMM(projtodate);
            var dtProjtoDate = convertToJSDateFormat(projtodate);

            if ((Date.parse(dtProjFromDate) > Date.parse(dtProjtoDate))) {
                alert("Project start date cannot be greater than end date");
                ScrollValidate('divEdu');
                document.getElementById('txtProjFromDt_' + (i - 1)).focus();
                document.getElementById('txtProjToDt_' + (i - 1)).focus();
                return false;
            }
        }     
    }

//    var txtperstrn = document.getElementById('txtperstrn').value;
//    if ((txtperstrn == null || txtperstrn == "") && SubType != "SAVED") {
//        alert("Please provide details");
//        ScrollValidate('divEdu');
//        document.getElementById('txtperstrn').focus();
//        return false;
//    }
//    var txtAreasImprove = document.getElementById('txtAreasImprove').value;
//    if ((txtAreasImprove == null || txtAreasImprove == "") && SubType != "SAVED") {
//        alert("Please provide details");
//        ScrollValidate('divEdu');
//        document.getElementById('txtAreasImprove').focus();
//        return false;
//    }
//    var txtCarObj = document.getElementById('txtCarObj').value;
//    if ((txtCarObj == null || txtCarObj == "") && SubType != "SAVED") {
//        alert("Please enter career objectives");
//        ScrollValidate('divEdu');
//        document.getElementById('txtCarObj').focus();
//        return false;
//    }
//    var txtExpections = document.getElementById('txtExpections').value;
//    if ((txtExpections == null || txtExpections == "") && SubType != "SAVED") {
//        alert("Please provide details");
//        ScrollValidate('divEdu');
//        return false;
//    }
    table = document.getElementById("tblPrevEmp");
    for (var i = 2, row; row = table.rows[i]; i++) {
        var txtPrevOrgName = document.getElementById('txtPrevOrgName_' + (i - 1)).value;
        var txtPrevlocation = document.getElementById('txtPrevlocation_' + (i - 1)).value;
        var txtPrevDesig = document.getElementById('txtPrevDesig_' + (i - 1)).value;
        var txtPrevEmpCode = document.getElementById('txtPrevEmpCode_' + (i - 1)).value;
        var fromdate = document.getElementById('txtPrevFromDt_' + (i - 1)).value;
        var todate = document.getElementById('txtPrevToDt_' + (i - 1)).value;
        var txtPrevStarting = document.getElementById('txtPrevStarting_' + (i - 1)).value;
        var txtPrevFinal = document.getElementById('txtPrevFinal_' + (i - 1)).value;
        var txtPrevLeaveReason = document.getElementById('txtPrevLeaveReason_' + (i - 1)).value;

        if(!(txtPrevStarting == null || txtPrevStarting == ""))
        {
            if (number.test(txtPrevStarting)) {
            }
            else {
                alert("Please enter valid salary");
                ScrollValidate('divEmp');
                document.getElementById('txtPrevStarting_' + (i - 1)).focus();
                return false;
            }
        }
        
        if(!(txtPrevFinal == null || txtPrevFinal == ""))
        {
            if (number.test(txtPrevFinal)) {
            }
            else {
                alert("Please enter valid salary");
                ScrollValidate('divEmp');
                document.getElementById('txtPrevFinal_' + (i - 1)).focus();
                return false;
            }
        }

        if (fromdate != null && fromdate != "") {
            fromdate = convertDateFormatMMMToMM(fromdate);
            var dtFromDate = convertToJSDateFormat(fromdate);
            if ((Date.parse(dtToday) < Date.parse(dtFromDate))) {
                alert("Employment start date cannot be greater than current date");
                ScrollValidate('divEmp');
                document.getElementById('txtPrevFromDt_' + (i - 1)).focus();
                return false;
            }
        }

        if (todate != null && todate != "") {
            todate = convertDateFormatMMMToMM(todate);
            var dttoDate = convertToJSDateFormat(todate);
            if ((Date.parse(dtToday) < Date.parse(dttoDate))) {
                alert("Employment end date cannot be greater than current date");
                ScrollValidate('divEmp');
                document.getElementById('txtPrevToDt_' + (i - 1)).focus();
                return false;
            }
        }

        var fromdate = document.getElementById('txtPrevFromDt_' + (i - 1)).value;
        var todate = document.getElementById('txtPrevToDt_' + (i - 1)).value;

        if (todate != null && todate != "" && fromdate != null && fromdate != "") {
            fromdate = convertDateFormatMMMToMM(fromdate);
            var dtFromDate = convertToJSDateFormat(fromdate);
            todate = convertDateFormatMMMToMM(todate);
            var dttoDate = convertToJSDateFormat(todate);
            if ((Date.parse(dtFromDate) > Date.parse(dttoDate))) {
                alert("Previous employment start date cannot be greater than end date");
                ScrollValidate('divEmp');
                document.getElementById('txtPrevFromDt_' + (i - 1)).focus();
                document.getElementById('txtPrevToDt_' + (i - 1)).focus();
                return false;
            }
            if (document.getElementById('txtPrevFromDt_' + i) != null) {
                var fromdateNext = document.getElementById('txtPrevFromDt_' + i).value;
                if (fromdateNext != null && fromdateNext != "") {
                    fromdateNext = convertDateFormatMMMToMM(fromdateNext);
                    var dtfromDateNext = convertToJSDateFormat(fromdateNext);
                    if (dtFromDate <=dtfromDateNext) {
                        alert("Employment dates for two different employments cannot overlap. Please enter valid dates");
                        ScrollValidate('divEmp');
                        document.getElementById('txtPrevFromDt_' + (i - 1)).focus();
                        document.getElementById('txtPrevFromDt_' + i).focus();
                        return false;
                    }
                }
            }

            if (document.getElementById('txtPrevToDt_' + i) != null) {
                var todateNext = document.getElementById('txtPrevToDt_' + i).value;
                if (todateNext != null && todateNext != "") {
                    todateNext = convertDateFormatMMMToMM(todateNext);
                    var dttoDateNext = convertToJSDateFormat(todateNext);
                    if ((Date.parse(dtFromDate) <= Date.parse(dttoDateNext))) {
                        alert("Employment dates for two different employments cannot overlap. Please enter valid dates");
                        ScrollValidate('divEmp');
                        document.getElementById('txtPrevFromDt_' + (i - 1)).focus();
                        document.getElementById('txtPrevToDt_' + i).focus();
                        return false;
                    }
                }
            }
        }

        if( fromdate != null && fromdate != "" && (todate == null || todate == "") )
        {
            alert("Both the Previous employment dates need to be filled.");
            ScrollValidate('divEmp');
            document.getElementById('txtPrevToDt_' + (i - 1)).focus();
            return false;
        }

        if( todate != null && todate != "" && (fromdate == null || fromdate == "") )
        {
            alert("Both the Previous employment dates need to be filled.");
            ScrollValidate('divEmp');
            document.getElementById('txtPrevFromDt_' + (i - 1)).focus();
            return false;
        }

        if(SubType != "SAVED")
        {
            var fromdate = document.getElementById('txtPrevFromDt_' + (i - 1)).value;
            var todate = document.getElementById('txtPrevToDt_' + (i - 1)).value;

            if( (txtPrevOrgName == null || txtPrevOrgName == "") &&
                (txtPrevlocation == null || txtPrevlocation == "") &&
                (txtPrevDesig == null || txtPrevDesig == "") &&
                (txtPrevEmpCode == null || txtPrevEmpCode == "") &&
                (fromdate == null || fromdate == "") &&
                (todate == null || todate == "") &&
                (txtPrevStarting == null || txtPrevStarting == "") &&
                (txtPrevFinal == null || txtPrevFinal == "") &&
                (txtPrevLeaveReason == null || txtPrevLeaveReason == "")
              )
            {
                //If all the fields are empty in a row; do not perform validation.
            }
            else //If atleast any one field in a row is filled; perform validation
            {
                if(txtPrevOrgName == null || txtPrevOrgName == "")
                {
                    alert("Please enter previous organisation name");
                    ScrollValidate('divEmp');
                    document.getElementById('txtPrevOrgName_' + (i - 1)).focus();
                    return false;
                }

                if(txtPrevlocation == null || txtPrevlocation == "")
                {
                    alert("Please enter organisation location");
                    ScrollValidate('divEmp');
                    document.getElementById('txtPrevlocation_' + (i - 1)).focus();
                    return false;
                }

                if(txtPrevDesig == null || txtPrevDesig == "")
                {
                    alert("Please enter designation");
                    ScrollValidate('divEmp');
                    document.getElementById('txtPrevDesig_' + (i - 1)).focus();
                    return false;
                }

                if(txtPrevEmpCode == null || txtPrevEmpCode == "")
                {
                    alert("Please enter employee code/no.");
                    ScrollValidate('divEmp');
                    document.getElementById('txtPrevEmpCode_' + (i - 1)).focus();
                    return false;
                }

                if(fromdate == null || fromdate == "")
                {
                    alert("Please enter employment start date");
                    ScrollValidate('divEmp');
                    document.getElementById('txtPrevFromDt_' + (i - 1)).focus();
                    return false;
                }

                if(todate == null || todate == "")
                {
                    alert("Please enter employment end date");
                    ScrollValidate('divEmp');
                    document.getElementById('txtPrevToDt_' + (i - 1)).focus();
                    return false;
                }

                if(txtPrevStarting == null || txtPrevStarting == "")
                {
                    alert("Please enter starting salary");
                    ScrollValidate('divEmp');
                    document.getElementById('txtPrevStarting_' + (i - 1)).focus();
                    return false;
                }

                if(txtPrevFinal == null || txtPrevFinal == "")
                {
                    alert("Please enter final salary");
                    ScrollValidate('divEmp');
                    document.getElementById('txtPrevFinal_' + (i - 1)).focus();
                    return false;
                }

                if(txtPrevLeaveReason == null || txtPrevLeaveReason == "")
                {
                    alert("Please enter leaving reason");
                    ScrollValidate('divEmp');
                    document.getElementById('txtPrevLeaveReason_' + (i - 1)).focus();
                    return false;
                }
            }
        }
    }


    table = document.getElementById("tblRef");
    for (var i = 1, row; row = table.rows[i]; i++) {
        var txtRefName = document.getElementById('txtRefName_' + i).value;
        var txtAssociation = document.getElementById('txtAssociation_' + i).value;
        var txtOrgDesig = document.getElementById('txtOrgDesig_' + i).value;
        var txtEmail = document.getElementById('txtEmail_' + i).value;
        var txtPhone = document.getElementById('txtPhone_' + i).value;
        var txtResidence = document.getElementById('txtResidence_' + i).value;
        
        if (txtEmail != "") {
            if (email.test(txtEmail)) {
            }
            else {
                alert("Please enter valid email address");
                ScrollValidate('divRef');
                document.getElementById('txtEmail_' + i).focus();
                return false;
            }
        }

        if (txtPhone != "") {
            if (landline.test(txtPhone)) {
            }
            else {
                alert("Please enter valid office phone number");
                ScrollValidate('divRef');
                document.getElementById('txtPhone_' + i).focus();
                return false;
            }
        }

        if (txtResidence != "") {
            if (landline.test(txtResidence)) {
            }
            else {
                alert("Please enter valid residence phone number");
                ScrollValidate('divRef');
                document.getElementById('txtResidence_' + i).focus();
                return false;
            }
        }
    

        if(SubType != "SAVED")
        {
            if( (txtRefName == null || txtRefName == "") &&
                (txtAssociation == null || txtAssociation == "") &&
                (txtOrgDesig == null || txtOrgDesig == "") &&
                (txtEmail == null || txtEmail == "") &&
                (txtPhone == null || txtPhone == "") &&
                (txtResidence == null || txtResidence == "")
              )
            {
                //If all the elements are empty in a row; do not perform validation.
            }

            else
            {
                if(txtRefName == null || txtRefName == "")
                {
                    alert("Please enter name");
                    ScrollValidate('divRef');
                    document.getElementById('txtRefName_' + i).focus();
                    return false;
                }

                if(txtAssociation == null || txtAssociation == "")
                {
                    alert("Please enter association details");
                    ScrollValidate('divRef');
                    document.getElementById('txtAssociation_' + i).focus();
                    return false;
                }

                if(txtOrgDesig == null || txtOrgDesig == "")
                {
                    alert("Please enter designation");
                    ScrollValidate('divRef');
                    document.getElementById('txtOrgDesig_' + i).focus();
                    return false;
                }

                if(txtEmail == null || txtEmail == "")
                {
                    alert("Please enter email address");
                    ScrollValidate('divRef');
                    document.getElementById('txtEmail_' + i).focus();
                    return false;
                }

                if(txtPhone == null || txtPhone == "")
                {
                    alert("Please enter office phone number");
                    ScrollValidate('divRef');
                    document.getElementById('txtPhone_' + i).focus();
                    return false;
                }

                if(txtResidence == null || txtResidence == "")
                {
                    alert("Please enter residence phone number");
                    ScrollValidate('divRef');
                    document.getElementById('txtResidence_' + i).focus();
                    return false;
                }
            }
        }
    }

    if ($('input:radio[name=illness]:checked').val() == "Y") {
        var illDet = document.getElementById("txtIllnessDet").value;
        if ((illDet == null || illDet == "") && SubType != "SAVED") {
            alert("Please enter illness details");
            ScrollValidate('divAdd');
            document.getElementById('txtIllnessDet').focus();
            return false;
        }
    }

    if ($('input:radio[name=passport]:checked').val() == "Y") {
        var txtpassportNo = document.getElementById("txtpassportNo").value;
        if ((txtpassportNo == null || txtpassportNo == "") && SubType != "SAVED") {
            alert("Please enter passport number");
            ScrollValidate('divAdd');
            document.getElementById("txtpassportNo").focus();
            return false;
        }
        var validdate = document.getElementById("dtValidtill").value;
        // debugger;
        if ((validdate == null || validdate == "") && SubType != "SAVED") {
            alert("Please enter passport validity date");
            ScrollValidate('divAdd');
            document.getElementById('dtValidtill').focus();
            document.getElementById("dtValidtill").value = "";
            return false;
        }
        if (validdate != null && validdate != "") {
            validdate = convertDateFormatMMMToMM(validdate);
            var dtDate = convertToJSDateFormat(validdate);
            validdate = new Date(dtDate);
            if ((Date.parse(dtToday) > Date.parse(validdate))) {
                alert("Passport validity date should be a future date");
                ScrollValidate('divAdd');
                document.getElementById("dtValidtill").value = "";
                document.getElementById("dtValidtill").focus();
                return false;
            }
        }
    }
    if ($('input:radio[name=apply]:checked').val() == "Y") {
        var txtLocation = document.getElementById("txtLocation").value;
        if ((txtLocation == null || txtLocation == "") && SubType != "SAVED") {
            alert("Please enter location details");
            ScrollValidate('divAdd');
            document.getElementById("txtLocation").focus();
            return false;
        }
        var txtDivision = document.getElementById("txtDivision").value;
        if ((txtDivision == null || txtDivision == "") && SubType != "SAVED") {
            alert("Please enter division");
            ScrollValidate('divAdd');
            document.getElementById("txtDivision").focus();
            return false;
        }
        var Interview = document.getElementById("dtInterview").value;
        if ((Interview == null || Interview == "") && SubType != "SAVED") {
            alert("Please enter interview date");
            ScrollValidate('divAdd');
            document.getElementById('dtInterview').focus();
            document.getElementById("dtInterview").value = "";
            return false;
        }
        if (Interview != null && Interview != "") {
            Interview = convertDateFormatMMMToMM(Interview);
            var dtInterViewDate = convertToJSDateFormat(Interview);
            Interview = new Date(dtInterViewDate);
            if ((Date.parse(dtToday) < Date.parse(Interview))) {
                alert("Interview date cannot be a future date");
                ScrollValidate('divAdd');
                document.getElementById("dtInterview").value = "";
                document.getElementById("dtInterview").focus();
                return false;
            }
        }
    }
    if ($('input:radio[name=relative]:checked').val() == "Y") {
        var relativeExp =new RegExp("^[a-zA-Z ]*$");    
        var txtRelativeName = document.getElementById("txtRelativeName").value;
        if ((txtRelativeName == null || txtRelativeName == "") && SubType != "SAVED") {
            alert("Please enter relative name");
            ScrollValidate('divAdd');
            document.getElementById("txtRelativeName").focus();
            return false;
        }
        if(txtRelativeName != null && txtRelativeName != ""){
            if (relativeExp.test(txtRelativeName)) {
            }
            else {
                alert("Please enter valid name");
                ScrollValidate('divAdd');
                document.getElementById('txtRelativeName').focus();
                return false;
            }
        }
        var txtRelativeLocation = document.getElementById("txtRelativeLocation").value;
        if ((txtRelativeLocation == null || txtRelativeLocation == "") && SubType != "SAVED") {
            alert("Please enter location");
            ScrollValidate('divAdd');
            document.getElementById("txtRelativeLocation").focus();
            return false;
        }
        var txtRelativeDivision = document.getElementById("txtRelativeDivision").value;
        if ((txtRelativeDivision == null || txtRelativeDivision == "") && SubType != "SAVED") {
            alert("Please enter division");
            ScrollValidate('divAdd');
            document.getElementById('txtRelativeDivision').focus();
            return false;
        }
    }
    if ($('input:radio[name=arrested]:checked').val() == "Y") {
        var txtOffense = document.getElementById("txtOffense").value;
        if ((txtOffense == null || txtOffense == "") && SubType != "SAVED") {
            alert("Please enter offense details");
            ScrollValidate('divAdd');
            document.getElementById("txtOffense").focus();
            return false;
        }
        var txtconviction = document.getElementById("txtconviction").value;
        if ((txtconviction == null || txtconviction == "") && SubType != "SAVED") {
            alert("Please enter conviction details");
            ScrollValidate('divAdd');
            document.getElementById("txtconviction").focus();
            return false;
        }
    }
    //debugger;
    if (($('#selLocation_1 :selected').text()) == "Select" && SubType != "SAVED") {
        alert("Please select 3 preferred locations");
        ScrollValidate('divAdd');
        return false;
    }
    if (($('#selLocation_2 :selected').text()) == "Select" && SubType != "SAVED") {
        alert("Please select 3 preferred locations");
        ScrollValidate('divAdd');
        return false;
    }
    if (($('#selLocation_3 :selected').text()) == "Select" && SubType != "SAVED") {
        alert("Please select 3 preferred locations");
        ScrollValidate('divAdd');
        return false;
    }

    selectlocations();
    var txtPlace = document.getElementById("txtPlace").value;
    if ((txtPlace == null || txtPlace == "") && SubType != "SAVED") {
        alert("Please enter place");
        ScrollValidate('divDec');
        document.getElementById("txtPlace").focus();
        return false;
    }
    var txtSign = document.getElementById("txtSign").value;
    if ((txtSign == null || txtSign == "") && SubType != "SAVED") {
        alert("Please enter your Name/Signature");
        ScrollValidate('divDec');
        document.getElementById("txtSign").focus();
        return false;
    }
}

function selectlocations() {

    if (($('#selLocation_1 :selected').text() != "Select" && $('#selLocation_2 :selected').text() != "Select") && ($('#selLocation_1 :selected').text() == $('#selLocation_2 :selected').text())) {
        alert("Please select 3 different locations");
        ScrollValidate('divAdd');
	$("#selLocation_2").prop('selectedIndex',0);
        return false;
    }
    else if (($('#selLocation_1 :selected').text() != "Select" && $('#selLocation_3 :selected').text() != "Select") && ($('#selLocation_1 :selected').text() == $('#selLocation_3 :selected').text())) {
        alert("Please select 3 different locations");
        ScrollValidate('divAdd');
	$("#selLocation_3").prop('selectedIndex',0);
        return false;
    }
    else if (($('#selLocation_2 :selected').text() == $('#selLocation_3 :selected').text()) && ($('#selLocation_2 :selected').text() != "Select" && $('#selLocation_3 :selected').text() != "Select")) {
        alert("Please select 3 different locations");
        ScrollValidate('divAdd');
	$("#selLocation_3").prop('selectedIndex',0);
        return false;
    }
    else if (($('#selLocation_2 :selected').text() == $('#selLocation_1 :selected').text() == $('#selLocation_2 :selected').text()) && ($('#selLocation_2 :selected').text() != "Select" && $('#selLocation_1 :selected').text() != "Select" && $('#selLocation_2 :selected').text() != "Select")) {
        alert("Please select 3 different locations");
        ScrollValidate('divAdd');
	$("#selLocation_2").prop('selectedIndex',0);
        return false;
    }
    UpdateProgressBar();
}

function addEducationRows() {
    // debugger;
    var table = document.getElementById("tbleducationdetails");
    var tableRow;
    var i = table.rows.length;
    tableRow = ['<tr>',
'<td width="10%" class="tdStyle"><input type="text" id="txtCourse_' + i + '" /></td>',
'<td width="18%" class="tdStyle"><input type="text" id="txtSepcialization_' + i + '" /></td>',

'<td width="18%" class="tdStyle"><textarea id="txtNameUni_' + i + '" ></textarea></td>',
'<td width="18%" class="tdStyle"><input type="text" id="txtlocation_' + i + '" /></td>',

'<td width="18%" class="tdStyle"><input type="text" id="txtgrad_' + i + '" /></td>',
'<td width="18%" class="tdStyle"><input type="text" id="txtAggregate_' + i + '" /></td>',

'</tr>'].join('');
    // $("#tbleducationdetails tr").last().after('<tr><td width="11%" class="tdStyle"><label id="SerNo_' + i + '></label></td><td width="11%" class="tdStyle"><select id="selCourse_' + i + '><option>Select</option></select></td><td width="11%" class="tdStyle"><input type="text" id="txtSepcialization_' + i + ' /></td><td width="11%" class="tdStyle"><textarea id="txtAdressUni' + i + ' ></textarea></td><td width="11%" class="tdStyle"><input type="text" id="txtFromDt_' + i + ' /></td><td width="11%" class="tdStyle"><input type="text" id="txtAggregate_' + i + ' /></td><td width="11%" class="tdStyle"><select id="selEduType_' + i + ' ><option>Select</option></select></td><td width="11%" class="tdStyle"><input type="text" id="txtSepcialization_' + i + ' /></td><td width="11%" class="tdStyle"><textarea id="txtRemarks_' + i + ' ></textarea></td></tr>');
    $("#tbleducationdetails").append(tableRow);


}

function removeEducationRows() {
    // debugger;
    var table = document.getElementById("tbleducationdetails");
    var tableRow;
    var i = table.rows.length; if (i > 5) {
        $('#tbleducationdetails tr:last-child').remove();
    }
}

function addTrainingRows() {
    //debugger;
    var table = document.getElementById("tblTraining");
    var tableRow;
    var i = table.rows.length;
    var id = i - 1;
    tableRow = ['<tr>',
'<td class="tdStyle"><input type="text"   id="txtTrainOrg_' + id + '" /></td>',
'<td class="tdStyle"><input type="text"    id="txtSkills_' + id + '" /></td>',
'<td  class="tdStyle"><input type="text"    id="txtTrainLocation_' + id + '" ></textarea></td>',
'<td  class="tdStyle"><input type="text" data-calendar="false" readonly="readonly" id="txtTrainFromDt_' + id + '" /></td>',
'<td  class="tdStyle"><input type="text" data-calendar="false" readonly="readonly" id="txtTrainToDt_' + id + '" /></td>',

'</tr>'].join('');
    // $("#tbleducationdetails tr").last().after('<tr><td width="11%" class="tdStyle"><label id="SerNo_' + i + '></label></td><td width="11%" class="tdStyle"><select id="selCourse_' + i + '><option>Select</option></select></td><td width="11%" class="tdStyle"><input type="text" id="txtSepcialization_' + i + ' /></td><td width="11%" class="tdStyle"><textarea id="txtAdressUni' + i + ' ></textarea></td><td width="11%" class="tdStyle"><input type="text" id="txtFromDt_' + i + ' /></td><td width="11%" class="tdStyle"><input type="text" id="txtAggregate_' + i + ' /></td><td width="11%" class="tdStyle"><select id="selEduType_' + i + ' ><option>Select</option></select></td><td width="11%" class="tdStyle"><input type="text" id="txtSepcialization_' + i + ' /></td><td width="11%" class="tdStyle"><textarea id="txtRemarks_' + i + ' ></textarea></td></tr>');
    $("#tblTraining").append(tableRow);

    for (var i = 1, row; row = table.rows[i]; i++) {
        $("#txtTrainFromDt_" + i).datepicker({ dateFormat: "dd-M-yy", changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            beforeShow: function (el, dp) {
                $('#ui-datepicker-div')[$(el).is('[data-calendar="false"]') ? 'removeClass' : 'addClass']('hide-calendar');
            }
        });
        $("#txtTrainToDt_" + i).datepicker({ dateFormat: "dd-M-yy", changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            beforeShow: function (el, dp) {
                $('#ui-datepicker-div')[$(el).is('[data-calendar="false"]') ? 'removeClass' : 'addClass']('hide-calendar');
            }
        });
    }
}

function removeTrainingRows() {
    // debugger;
    var table = document.getElementById("tblTraining");
    var tableRow;
    var i = table.rows.length; if (i > 2) {
        $('#tblTraining tr:last-child').remove();
    }
}

function addProjectRows() {
    //debugger;
    var table = document.getElementById("tblProject");
    var tableRow;
    var i = table.rows.length;
    var id = i - 1;
    tableRow = ['<tr>',
'<td class="tdStyle"><input type="text"   id="txtProjName_' + id + '" /></td>',
'<td class="tdStyle"><input type="text"    id="txtProjSkills_' + id + '" /></td>',
'<td class="tdStyle"><input type="text"    id="txtProjDesc_' + id + '" /></td>',
'<td  class="tdStyle"><input type="text"    id="txtProjLocation_' + id + '" ></td>',
'<td  class="tdStyle"><input type="text" data-calendar="false" readonly="readonly" id="txtProjFromDt_' + id + '" /></td>',
'<td  class="tdStyle"><input type="text" data-calendar="false" readonly="readonly" id="txtProjToDt_' + id + '" /></td>',

'</tr>'].join('');
    
    $("#tblProject").append(tableRow);

    for (var i = 1, row; row = table.rows[i]; i++) {
        $("#txtProjFromDt_" + i).datepicker({ dateFormat: "dd-M-yy", changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            beforeShow: function (el, dp) {
                $('#ui-datepicker-div')[$(el).is('[data-calendar="false"]') ? 'removeClass' : 'addClass']('hide-calendar');
            }
        });
        $("#txtProjToDt_" + i).datepicker({ dateFormat: "dd-M-yy", changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            beforeShow: function (el, dp) {
                $('#ui-datepicker-div')[$(el).is('[data-calendar="false"]') ? 'removeClass' : 'addClass']('hide-calendar');
            }
        });
    }
}

function removeProjectRows() {
    // debugger;
    var table = document.getElementById("tblProject");
    var tableRow;
    var i = table.rows.length; if (i > 2) {
        $('#tblProject tr:last-child').remove();
    }
}

function addPrevEmpRows() {
    //debugger;
    var table = document.getElementById("tblPrevEmp");
    var tableRow;
    var i = table.rows.length;
    var id = i - 1;
    tableRow = ['<tr>',
'<td width="4%" class="tdStyle">' + id + '</td>',
'<td width="15%" class="tdStyle"><input type="text" style="width:90%"  id="txtPrevOrgName_' + id + '" /></td>',
'<td width="10%" class="tdStyle"><input type="text" style="width:90%" id="txtPrevlocation_' + id + '" /></td>',
'<td width="10%" class="tdStyle"><input type="text" style="width:90%" id="txtPrevDesig_' + id + '" /></td>',
'<td width="10%" class="tdStyle"><input type="text" style="width:90%" id="txtPrevEmpCode_' + id + '" /></td>',
'<td  class="tdStyle"><input type="text" data-calendar="false" readonly="readonly" style="width:90%" id="txtPrevFromDt_' + id + '" /></td>',
'<td  class="tdStyle"><input type="text" data-calendar="false" readonly="readonly" style="width:90%" id="txtPrevToDt_' + id + '" /></td>',
'<td class="tdStyle"><input type="text" style="width:90%" id="txtPrevStarting_' + id + '" /></td>',
'<td class="tdStyle"><input type="text" style="width:90%" id="txtPrevFinal_' + id + '" /></td>',
'<td  class="tdStyle"><input type="text" style="width:90%" id="txtPrevLeaveReason_' + id + '" /></td>',



'</tr>'].join('');
    // $("#tbleducationdetails tr").last().after('<tr><td width="11%" class="tdStyle"><label id="SerNo_' + i + '></label></td><td width="11%" class="tdStyle"><select id="selCourse_' + i + '><option>Select</option></select></td><td width="11%" class="tdStyle"><input type="text" id="txtSepcialization_' + i + ' /></td><td width="11%" class="tdStyle"><textarea id="txtAdressUni' + i + ' ></textarea></td><td width="11%" class="tdStyle"><input type="text" id="txtFromDt_' + i + ' /></td><td width="11%" class="tdStyle"><input type="text" id="txtAggregate_' + i + ' /></td><td width="11%" class="tdStyle"><select id="selEduType_' + i + ' ><option>Select</option></select></td><td width="11%" class="tdStyle"><input type="text" id="txtSepcialization_' + i + ' /></td><td width="11%" class="tdStyle"><textarea id="txtRemarks_' + i + ' ></textarea></td></tr>');
    $("#tblPrevEmp").append(tableRow);

    for (var i = 1, row; row = table.rows[i]; i++) {
        $("#txtPrevFromDt_" + i).datepicker({ dateFormat: "dd-M-yy", changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            beforeShow: function (el, dp) {
                $('#ui-datepicker-div')[$(el).is('[data-calendar="false"]') ? 'removeClass' : 'addClass']('hide-calendar');
            }
        });
        $("#txtPrevToDt_" + i).datepicker({ dateFormat: "dd-M-yy", changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            beforeShow: function (el, dp) {
                $('#ui-datepicker-div')[$(el).is('[data-calendar="false"]') ? 'removeClass' : 'addClass']('hide-calendar');
            }
        });
    }
}



function removePrevEmpRows() {
    // debugger;
    var table = document.getElementById("tblPrevEmp");
    var tableRow;
    var i = table.rows.length; if (i > 2) {
        $('#tblPrevEmp tr:last-child').remove();
    }
}

function addtblRefRows() {
    //debugger;
    var table = document.getElementById("tblRef");
    var tableRow;
    var i = table.rows.length;
    tableRow = ['<tr>',
'<td width="4%" class="tdStyle">' + i + '</td>',
'<td width="16%" class="tdStyle"><input type="text" style="width:90%"  id="txtRefName_' + i + '" /></td>',
'<td width="16%" class="tdStyle"><input type="text" style="width:90%"  id="txtAssociation_' + i + '" /></td>',
'<td width="16%" class="tdStyle"><input type="text" style="width:90%"  id="txtOrgDesig_' + i + '" /></td>',
'<td width="16%" class="tdStyle"><input type="text" style="width:90%"  id="txtEmail_' + i + '" /></td>',
'<td width="16%" class="tdStyle"><input type="text" style="width:90%"  id="txtPhone_' + i + '" /></td>',
'<td width="16%" class="tdStyle"><input type="text" style="width:90%" id="txtResidence_' + i + '" /></td>',




'</tr>'].join('');
    // $("#tbleducationdetails tr").last().after('<tr><td width="11%" class="tdStyle"><label id="SerNo_' + i + '></label></td><td width="11%" class="tdStyle"><select id="selCourse_' + i + '><option>Select</option></select></td><td width="11%" class="tdStyle"><input type="text" id="txtSepcialization_' + i + ' /></td><td width="11%" class="tdStyle"><textarea id="txtAdressUni' + i + ' ></textarea></td><td width="11%" class="tdStyle"><input type="text" id="txtFromDt_' + i + ' /></td><td width="11%" class="tdStyle"><input type="text" id="txtAggregate_' + i + ' /></td><td width="11%" class="tdStyle"><select id="selEduType_' + i + ' ><option>Select</option></select></td><td width="11%" class="tdStyle"><input type="text" id="txtSepcialization_' + i + ' /></td><td width="11%" class="tdStyle"><textarea id="txtRemarks_' + i + ' ></textarea></td></tr>');
    $("#tblRef").append(tableRow);

}

function AddGapTableRows(rowId,calledFrom,data) {
    //debugger;
    var table = document.getElementById("tblgapDetails");
    var tableRow;
    var i = table.rows.length;
    var gapDesc;
    var gapFromDate;
    var gapToDate; 
    var gapDesc;
    var gapReason;
    var hdnConfiguredGap=document.getElementById('hdnGap').value;
    var txtGapFromDate=document.getElementById('txtGapFromDate_'+rowId);
    if(calledFrom=="validate" && document.getElementById('SelCourse_'+rowId).value!="Select" && (txtGapFromDate=="" || txtGapFromDate=="" || txtGapFromDate==undefined)){
        var dateFrom = document.getElementById('dtToGrad_'+rowId).value;   
        var dateTo = document.getElementById('dtFromGrad_'+(parseInt(rowId)+1)).value;  
        var fromArray = dateFrom.split('/');   
        var toArray = dateTo.split('/');
       if (parseInt(toArray[1]) - parseInt(fromArray[1]) >= parseInt(hdnConfiguredGap)) {
            gapDesc="Gap between "+document.getElementById('SelCourse_'+rowId).value+" and "+document.getElementById('SelCourse_'+(parseInt(rowId)+1)).value;   
            gapFromDate=document.getElementById('dtToGrad_'+rowId).value;   
            gapToDate=document.getElementById('dtFromGrad_'+ (parseInt(rowId)+1)).value;
              tableRow = ['<tr>',

'<td width="16%" class="tdStyle"><input type="text" readonly="readonly" id="txtGapDesc_' + rowId + '" value="'+gapDesc+'"/></td>',
'<td width="16%" class="tdStyle"><input type="text" readonly="readonly"  id="txtGapFromDate_' + rowId + '" value="'+gapFromDate+'"  /></td>',
'<td width="16%" class="tdStyle"><input type="text" readonly="readonly" id="txtGapToDate_' + rowId + '" value="'+gapToDate+'" /></td>',
'<td width="16%" class="tdStyle"><input type="text"  id="txtGapReason' + rowId + '" /></td>',
'<input type="hidden" id="hdnGapRownum_'+rowId+'" value="'+ i +'" />',
'</tr>'].join('');  
        }
    }
    else if(calledFrom=="getdata"){
        gapDesc="Gap between "+data.Education[parseInt(rowId)-1].degree+" and "+data.Education[parseInt(rowId)].degree;   
        gapFromDate=data.Education[parseInt(rowId)-1].toDate;   
        gapToDate=data.Education[parseInt(rowId)].fromDate;   
        gapReason=data.Education[parseInt(rowId)-1].gapReason;  
          tableRow = ['<tr>',

'<td width="16%" class="tdStyle"><input type="text" readonly="readonly" id="txtGapDesc_' + rowId + '" value="'+gapDesc+'"/></td>',
'<td width="16%" class="tdStyle"><input type="text" readonly="readonly"  id="txtGapFromDate_' + rowId + '" value="'+gapFromDate+'"  /></td>',
'<td width="16%" class="tdStyle"><input type="text" readonly="readonly" id="txtGapToDate_' + rowId + '" value="'+gapToDate+'" /></td>',
'<td width="16%" class="tdStyle"><input type="text"  id="txtGapReason' + rowId + '" value="'+gapReason+'" /></td>',
'<input type="hidden" id="hdnGapRownum_'+rowId+'" value="'+ i +'" />',
'</tr>'].join('');  
    }
       
    $("#tblgapDetails").append(tableRow);

}



function removetblRefRows() {
    // debugger;
    var table = document.getElementById("tblRef");
    var tableRow;
    var i = table.rows.length; if (i > 1) {
        $('#tblRef tr:last-child').remove();
    }
}


function DisableDetailsSection(id, rdId, type) {
    //debugger;
    if (id == 'illness') {

        if ($('#' + rdId.id).is(':checked') && type == 'No') {
            ($('#txtIllnessDet').attr("disabled", "disabled"));
        }
        else {
            ($("#txtIllnessDet").removeAttr("disabled"));
        }
    }
    else if (id == 'passport') {
        if ($('#' + rdId.id).is(':checked') && type == 'No') {
            ($('#txtpassportNo').attr("disabled", "disabled"));
            ($('#dtValidtill').attr("disabled", "disabled"));
        }
        else {
            ($("#txtpassportNo").removeAttr("disabled"));
            ($('#dtValidtill').removeAttr("disabled"));
        }
    }
    else if (id == 'apply') {
        if ($('#' + rdId.id).is(':checked') && type == 'No') {
            ($('#dtInterview').attr("disabled", "disabled"));
            ($('#txtLocation').attr("disabled", "disabled"));
            ($('#txtDivision').attr("disabled", "disabled"));
        }
        else {
            ($('#dtInterview').removeAttr("disabled"));
            ($('#txtLocation').removeAttr("disabled"));
            ($('#txtDivision').removeAttr("disabled"));
        }
    }
    else if (id == 'relative') {
        if ($('#' + rdId.id).is(':checked') && type == 'No') {
            ($('#txtRelativeName').attr("disabled", "disabled"));
            ($('#txtRelativeLocation').attr("disabled", "disabled"));
            ($('#txtRelativeDivision').attr("disabled", "disabled"));
        }
        else {
            ($('#txtRelativeName').removeAttr("disabled"));
            ($('#txtRelativeLocation').removeAttr("disabled"));
            ($('#txtRelativeDivision').removeAttr("disabled"));
        }
    }
    else if (id == 'arrested') {
        if ($('#' + rdId.id).is(':checked') && type == 'No') {
            ($('#txtOffense').attr("disabled", "disabled"));
            ($('#txtconviction').attr("disabled", "disabled"));
        }
        else {
            ($('#txtOffense').removeAttr("disabled"));
            ($('#txtconviction').removeAttr("disabled"));
        }
    }
    UpdateProgressBar();
}

function UpdateProgressBar() {
    //  debugger;
    var currentCount = 0;
    var mandInputCount = 39;
    var rdIllnessYes = document.getElementById('rdIllnessYes');
    var rdPassYes = document.getElementById('rdPassYes');
    var rdApplyYes = document.getElementById('rdApplyYes');
    var rdRelativeYes = document.getElementById('rdRelativeYes');
    var rdArrestedYes = document.getElementById('rdArrestedYes');
 
    if (rdIllnessYes.checked == true) {
        mandInputCount = eval(mandInputCount) + 1;
    }
    if (rdPassYes.checked == true) {
        mandInputCount = eval(mandInputCount) + 2;
    }
    if (rdApplyYes.checked == true) {
        mandInputCount = eval(mandInputCount) + 3;
    }
    if (rdRelativeYes.checked == true) {
        mandInputCount = eval(mandInputCount) + 3;
    }
    if (rdArrestedYes.checked == true) {
        mandInputCount = eval(mandInputCount) + 2;
    }
     if ($('#SelCourse_4 :selected').text() != "Select") {    
        mandInputCount=eval(mandInputCount)+7;
    }

    var progressPercentage = 100 / eval(mandInputCount);

    if (rdIllnessYes.checked == true) {
        if (document.getElementById('txtIllnessDet').value != "") {
            currentCount = currentCount + progressPercentage;
        }
    }
    if (rdPassYes.checked == true) {
        if (document.getElementById('txtpassportNo').value != "") {
            currentCount = currentCount + progressPercentage;
        }
        if (document.getElementById('dtValidtill').value != "") {
            currentCount = currentCount + progressPercentage;
        }
    }
    if (rdApplyYes.checked == true) {
        if (document.getElementById('dtInterview').value != "") {
            currentCount = currentCount + progressPercentage;
        }
        if (document.getElementById('txtLocation').value != "") {
            currentCount = currentCount + progressPercentage;
        }
        if (document.getElementById('txtDivision').value != "") {
            currentCount = currentCount + progressPercentage;
        }
    }
    if (rdRelativeYes.checked == true) {
        if (document.getElementById('txtRelativeName').value != "") {
            currentCount = currentCount + progressPercentage;
        }
        if (document.getElementById('txtRelativeLocation').value != "") {
            currentCount = currentCount + progressPercentage;
        }
        if (document.getElementById('txtRelativeDivision').value != "") {
            currentCount = currentCount + progressPercentage;
        }
    }
    if (rdArrestedYes.checked == true) {
        if (document.getElementById('txtOffense').value != "") {
            currentCount = currentCount + progressPercentage;
        }
        if (document.getElementById('txtconviction').value != "") {
            currentCount = currentCount + progressPercentage;
        }
    }
    if ((document.getElementById('chkMale').checked == true) || (document.getElementById('chkFemale').checked == true)) {
        currentCount = currentCount + progressPercentage;
    }
    if (document.getElementById('rdIndian').checked == true) {
        currentCount = currentCount + progressPercentage;
    }
    else if (document.getElementById('rdOthers').checked == true) {
        if ($('#ddlCountries :selected').text() != "Select") {
            currentCount = currentCount + progressPercentage;
        }
    }
    if (document.getElementById('txtMobile').value != "") {
        currentCount = currentCount + progressPercentage;
    }
    if (document.getElementById('txtAltMobile').value != "") {
        currentCount = currentCount + progressPercentage;
    }
    if (document.getElementById('txtAddress').value != "") {
        currentCount = currentCount + progressPercentage;
    }
//    if (document.getElementById('txtAddress2').value != "") {
//        currentCount = currentCount + progressPercentage;
//    }
    if (document.getElementById('txtState').value != "") {
        currentCount = currentCount + progressPercentage;
    }
    if (document.getElementById('txtCity').value != "") {
        currentCount = currentCount + progressPercentage;
    }
    if (document.getElementById('txtPin').value != "") {
        currentCount = currentCount + progressPercentage;
    }
//    if (document.getElementById('txtLandline').value != "") {
//        currentCount = currentCount + progressPercentage;
//    }
    if (document.getElementById('txtPerAddress').value != "") {
        currentCount = currentCount + progressPercentage;
    }
//    if (document.getElementById('txtPerAddress2').value != "") {
//        currentCount = currentCount + progressPercentage;
//    }
    if (document.getElementById('txtPerState').value != "") {
        currentCount = currentCount + progressPercentage;
    }
    if (document.getElementById('txtPerCity').value != "") {
        currentCount = currentCount + progressPercentage;
    }
    if (document.getElementById('txtPerPin').value != "") {
        currentCount = currentCount + progressPercentage;
    }
//    if (document.getElementById('txtPerPhone').value != "") {
//        currentCount = currentCount + progressPercentage;
//    }
    if ($('#SelCourse_1 :selected').text() != "Select") {
        currentCount = currentCount + progressPercentage;
    }   
    if (document.getElementById('txtNameUni_1').value != "") {
        currentCount = currentCount + progressPercentage;
    }
    if (document.getElementById('txtlocation_1').value != "") {
        currentCount = currentCount + progressPercentage;
    }   
     if (document.getElementById('dtFromGrad_1').value != "") {        
        currentCount = currentCount + progressPercentage;
    }
    if (document.getElementById('dtToGrad_1').value != "") {        
        currentCount = currentCount + progressPercentage;
    }
    if (document.getElementById('txtAggregate_1').value != "") {
        currentCount = currentCount + progressPercentage;
    }
    if ($('#SelCourse_2 :selected').text() != "Select") {
        currentCount = currentCount + progressPercentage;
    }   
    if (document.getElementById('txtNameUni_2').value != "") {
        currentCount = currentCount + progressPercentage;
    }
    if (document.getElementById('txtlocation_2').value != "") {
        currentCount = currentCount + progressPercentage;
    }   
    if (document.getElementById('dtToGrad_2').value != "") {        
        currentCount = currentCount + progressPercentage;
    }   
    if (document.getElementById('dtFromGrad_2').value != "") {        
        currentCount = currentCount + progressPercentage;
    }   
    if (document.getElementById('txtAggregate_2').value != "") {
        currentCount = currentCount + progressPercentage;
    }
    if ($('#SelCourse_3 :selected').text() != "Select") {
        currentCount = currentCount + progressPercentage;
    }
    if ($('#SelBranch_3 :selected').text() != "Select") {        
        currentCount = currentCount + progressPercentage;
    }
         if (document.getElementById('txtUniversity_3').value != "") {
        currentCount = currentCount + progressPercentage;
    }     
    if (document.getElementById('txtNameUni_3').value != "") {
        currentCount = currentCount + progressPercentage;
    }
    if (document.getElementById('txtlocation_3').value != "") {
        currentCount = currentCount + progressPercentage;
    }
      if (document.getElementById('dtToGrad_3').value != "") {        
        currentCount = currentCount + progressPercentage;
    }    
     if (document.getElementById('dtFromGrad_3').value != "") {        
        currentCount = currentCount + progressPercentage;
    }    
    if (document.getElementById('txtAggregate_3').value != "") {
        currentCount = currentCount + progressPercentage;
    }
    if ((document.getElementById('rdMathsYes').checked == true) || (document.getElementById('rdMathsNo').checked == true)) {
        currentCount = currentCount + progressPercentage;
    }
    if ((document.getElementById('rdEduDecYes').checked == true) || (document.getElementById('rdEduDecNo').checked == true)) {
        currentCount = currentCount + progressPercentage;
    }
//    if (document.getElementById('txtperstrn').value != "") {
//        currentCount = currentCount + progressPercentage;
//    }
//    if (document.getElementById('txtAreasImprove').value != "") {
//        currentCount = currentCount + progressPercentage;
//    }
//    if (document.getElementById('txtCarObj').value != "") {
//        currentCount = currentCount + progressPercentage;
//    }
//    if (document.getElementById('txtExpections').value != "") {
//        currentCount = currentCount + progressPercentage;
//    }
    if (document.getElementById('txtPlace').value != "") {
        currentCount = currentCount + progressPercentage;
    }
    if (document.getElementById('txtSign').value != "") {
        currentCount = currentCount + progressPercentage;
    }
    if ($('#selLocation_1 :selected').text() != "Select") {
        currentCount = currentCount + progressPercentage;
    }
    if ($('#selLocation_2 :selected').text() != "Select") {
        currentCount = currentCount + progressPercentage;
    }
    if ($('#selLocation_3 :selected').text() != "Select") {
        currentCount = currentCount + progressPercentage;
    } 
    if($('#SelCourse_4 :selected').text()!="Select"){
        if($('#SelBranch_4 :selected').text()!="Select" && $('#SelBranch_4 :selected').text()!=""){
            currentCount = currentCount + progressPercentage;
        }
        if(document.getElementById("txtUniversity_4").value!=""){
            currentCount = currentCount + progressPercentage;
        }        
        if(document.getElementById("txtNameUni_4").value!=""){
            currentCount = currentCount + progressPercentage;
        }
        if(document.getElementById("txtlocation_4").value!=""){
            currentCount = currentCount + progressPercentage;
        }
        if(document.getElementById("dtFromGrad_4").value!=""){
            currentCount = currentCount + progressPercentage;
        }
        if(document.getElementById("dtToGrad_4").value!=""){
            currentCount = currentCount + progressPercentage;
        }
         if(document.getElementById("txtAggregate_4").value!=""){
            currentCount = currentCount + progressPercentage;
        }
    }
    
    $("#progressbar").progressbar({
        value: parseInt(currentCount)
    });
    if (document.getElementById('lblProgressPerc').textContent == null)
        document.getElementById('lblProgressPerc').innerText = Math.round(currentCount) + "%";
    else
        document.getElementById('lblProgressPerc').textContent = Math.round(currentCount) + "%";
}




$(function () {
    // debugger;
    $('form').on('submit', function (e) {
        e.preventDefault();
        var subType = document.getElementById('hdnSubType').value;
        Save(subType);
    });

});

function Save(action) {
    //    debugger;    
    $('#loadingmessage').show();
    var Address = [];
    Address.push({ "address": document.getElementById('txtAddress').value,
        //"address2": document.getElementById('txtAddress2').value,
        "state": document.getElementById('txtState').value,
        "city": document.getElementById('txtCity').value,
        "pin": document.getElementById('txtPin').value,
        "landline": document.getElementById('txtLandline').value,
        "type": "C"
    });

    Address.push({ "address": document.getElementById('txtPerAddress').value,
        //"address2": document.getElementById('txtPerAddress2').value,
        "state": document.getElementById('txtPerState').value,
        "city": document.getElementById('txtPerCity').value,
        "pin": document.getElementById('txtPerPin').value,
        "landline": document.getElementById('txtPerPhone').value,
        "type": "P"
    });

    var Education = [];
    var table = document.getElementById("tbleducationdetails");
    var edSpecialization = "";
    var i = table.rows.length;
    for (var j = 1; j < i; j++) {
        //debugger;    
        var arrTo = document.getElementById('dtToGrad_' + j).value.split('/');
        var deg = $('#SelCourse_' + j + ' :selected').text();        
            if (deg == "B.Arch")
                deg = "BARCH";
            else if (deg == "B Sc")
                deg = "BSC";
            else if (deg == "BE / BTech")
                deg = "BE";
            else if (deg == "B Com")
                deg = "BCOM";
            else if (deg == "MTech")
                deg = "MTECH";
            else if (deg == "M Sc")
                deg = "MSC";
            else if (deg == "Phd")
                deg = "PHD";
            else if (deg == "Diploma")
                deg = "DIP";
            else if (deg == "Full Time M.Tech")
                deg = "MTECH";
            else if (deg == "Full Time ME")
                deg = "ME";
            else if (deg == "Integrated M.Tech")
                deg = "INMTECH";
            else if (deg == "Integrated ME")
                deg = "INME";
            else if (deg == "Integrated M.Sc")
                deg = "INMSC";

            if (j < 3) {
                edSpecialization = document.getElementById('txtBranch_' + j).value;
            }
            else {
                edSpecialization = $('#SelBranch_' + j + ' :selected').val();
            }    
            if(edSpecialization=="Select")
                edSpecialization="";        
            var gapReason=document.getElementById('txtGapReason' + j);
            var gapReasonValue="";
            if(gapReason!=undefined && gapReason!=null)
                gapReasonValue=document.getElementById('txtGapReason' + j).value;

            Education.push({ "qualify": document.getElementById('lblQualification_' + j).innerHTML,
                "degree": deg,
                "specialization": edSpecialization,
                "uniName": document.getElementById('txtNameUni_' + j).value,
                "university":document.getElementById('txtUniversity_' + j).value, //added by chaithra
                "location": document.getElementById('txtlocation_' + j).value,
                "month": arrTo[0],
                "year": arrTo[1],
                "fromDate": document.getElementById('dtFromGrad_' + j).value,
                "toDate": document.getElementById('dtToGrad_' + j).value,
                "aggregate": document.getElementById('txtAggregate_' + j).value,
                "gapReason": gapReasonValue,
            });
        }
    
    var ref = [];
    table = document.getElementById("tblRef");
    i = table.rows.length;
    for (var j = 1; j < i; j++) {
        ref.push({ "refname": document.getElementById('txtRefName_' + j).value,
            "association": document.getElementById('txtAssociation_' + j).value,
            "orgDesig": document.getElementById('txtOrgDesig_' + j).value,
            "email": document.getElementById('txtEmail_' + j).value,
            "phone": document.getElementById('txtPhone_' + j).value,
            "residence": document.getElementById('txtResidence_' + j).value
        });
    }
    var PrevEmp = [];
    table = document.getElementById("tblPrevEmp");
    i = table.rows.length;
    for (var j = 1; j < i - 1; j++) {
        PrevEmp.push({ "prevOrgName": document.getElementById('txtPrevOrgName_' + j).value,
            "prevLocation": document.getElementById('txtPrevlocation_' + j).value,
            "prevDesig": document.getElementById('txtPrevDesig_' + j).value,
            "prevEmpCode": document.getElementById('txtPrevEmpCode_' + j).value,
            "prevFromDt": document.getElementById('txtPrevFromDt_' + j).value,
            "prevToDt": document.getElementById('txtPrevToDt_' + j).value,
            "prevStSal": document.getElementById('txtPrevStarting_' + j).value,
            "prevEndSal": document.getElementById('txtPrevFinal_' + j).value,
            "prevLeaveRsn": document.getElementById('txtPrevLeaveReason_' + j).value
        });
    }
    var train = [];
    table = document.getElementById("tblTraining");
    i = table.rows.length;
    for (var j = 1; j < i - 1; j++) {
        train.push({ "trainOrg": document.getElementById('txtTrainOrg_' + j).value,
            "trainSkills": document.getElementById('txtSkills_' + j).value,
            "trainLocation": document.getElementById('txtTrainLocation_' + j).value,
            "trainStartDt": document.getElementById('txtTrainFromDt_' + j).value,
            "trainEndDt": document.getElementById('txtTrainToDt_' + j).value

        });
    }
    var Proj = [];
    table = document.getElementById("tblProject");
    i = table.rows.length;
    for (var j = 1; j < i - 1; j++) {
        Proj.push({ "projName": document.getElementById('txtProjName_' + j).value,
            "projSkills": document.getElementById('txtProjSkills_' + j).value,
            "projDescription": document.getElementById('txtProjDesc_' + j).value,
            "projLocation": document.getElementById('txtProjLocation_' + j).value,
            "projStartDt": document.getElementById('txtProjFromDt_' + j).value,
            "projEndDt": document.getElementById('txtProjToDt_' + j).value

        });
    }
    var PassPort = [];
    PassPort.push({ "ValidPass": $('input:radio[name=passport]:checked').val(),
        "PassPortNo": document.getElementById('txtpassportNo').value,
        "Validtill": document.getElementById('dtValidtill').value
    });
    var WiproApplied = [];
    WiproApplied.push({ "ValidApply": $('input:radio[name=apply]:checked').val(),
        "InterviewDate": document.getElementById('dtInterview').value,
        "InterviewLocation": document.getElementById('txtLocation').value,
        "InterviewDivision": document.getElementById('txtDivision').value
    });
    var relativeWipro = [];
    relativeWipro.push({ "Relative": $('input:radio[name=relative]:checked').val(),
        "RelativeName": document.getElementById('txtRelativeName').value,
        "RelativeLocation": document.getElementById('txtRelativeLocation').value,
        "RelativeDivision": document.getElementById('txtRelativeDivision').value
    });
    var Illness = [];
    Illness.push({ "Ill": $('input:radio[name=illness]:checked').val(),
        "IllnessDet": document.getElementById('txtIllnessDet').value
    })
    var Arrested = [];
    Arrested.push({ "Arrest": $('input:radio[name=arrested]:checked').val(),
        "Conviction": document.getElementById('txtconviction').value,
        "Offense": document.getElementById('txtOffense').value

    });
    var Locations = [];
    Locations.push({ "Location1": $('#selLocation_1 :selected').text(),
        "Location2": $('#selLocation_2 :selected').text(),
        "Location3": $('#selLocation_3 :selected').text()

    });
    var resno = document.getElementById('lblResumeNo').innerHTML;
    // validate();
    // debugger;
    var nationality;
    if ($('input:radio[name=Nation]:checked').val() == "OTH")
        nationality = $('#ddlCountries :selected').val()
    else
        nationality = $('input:radio[name=Nation]:checked').val();
    if(nationality=="Select")
        nationality="";
    var res = document.getElementById('lblResumeNo').innerHTML;
    var resarr = [];
    resarr = res.split(":");
    res = resarr[1];
//    var chkMaths = "N";
//    if (document.getElementById('chkMaths').checked == true)
//        chkMaths = "Y";
//    var chkEduDeclaration = "N";
//    if (document.getElementById('chkEduDeclaration').checked == true)
//        chkEduDeclaration = "Y";
    var Data = {
        "Resumeno": res,
        "Action": action,
        "firstname": document.getElementById('lblFirstName').value,
        "lastname": document.getElementById('lblLastName').value,
        "middlename": document.getElementById('txtMiddleName').value,
        "nationality": nationality,
        "gender": $('input:radio[name=gender]:checked').val(),
        "dob": document.getElementById('lblBirth').value,
        "AadhaarNo": document.getElementById('txtAadhaar').value,
        "Emailid": document.getElementById('lblEmail1').value,
        "mobileNo": document.getElementById('txtMobile').value,
        "altmobileNo": document.getElementById('txtAltMobile').value,
        "Address": Address,
        "Education": Education,
        "Training": train,
        "Project" : Proj,
        "PrevEmp": PrevEmp,
        "Reference": ref,
        "Illness": Illness,
        "PassPort": PassPort,
        "WiproApplied": WiproApplied,
        "RelativeInWipro": relativeWipro,
        //"AddInfo": document.getElementById('txtAddInfo').value,
        "Arrested": Arrested,
        "Locations": Locations,
        //"SignificantAchivement": document.getElementById('txtSignAchive').value,
        //"OtherInterests": document.getElementById('txtOtherInterests').value,
        //"PreStrengths": document.getElementById('txtperstrn').value,
        //"AreaImprove": document.getElementById('txtAreasImprove').value,
        //"CarObj": document.getElementById('txtCarObj').value,
        //"Expectations": document.getElementById('txtExpections').value,
        "SubmitDate": document.getElementById('dtToday').value,
        "Place": document.getElementById('txtPlace').value,
        "Signature": document.getElementById('txtSign').value,
        "maths": $('input:radio[name=Maths]:checked').val(),
        "EduDeclaration": $('input:radio[name=EduDeclaration]:checked').val(),
        "EduGap":document.getElementById('hdnGap').value

    };
    $.ajax({

        type: "POST",
        //url: "http://pwbsqa.wipro.com/cwbs/services/CWBS_CAM.svc/SaveData",
        url: "https://synergy.wipro.com/cwbsapp/services/CWBS_CAM.svc/SaveData",   
        //url: "http://localhost:62349/Services/CWBS_CAM.svc/SaveData",
         
        data: JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: true,
        success: function (result) {
            //alert("Success");

            $('#loadingmessage').hide();
            if (action == "SAVED") {
                alert("Data Saved Successfully");
                getdata(res, "N", "DECODED");
            }
            else if (action == "SUBMITED") {
                ($("#lblMsgSuccess").css('display', 'block'));
                $('#form1').css('display', 'none');
            }
        },

        error: function OnError(request, result, error) {
            $('#loadingmessage').hide();
            alert("Some Error Occured");

        }

    });

}

function CopyAdress() {
    var chkCopyAddres = document.getElementById('chkCopyAddres');
    if (chkCopyAddres.checked == true) {
        document.getElementById('txtPerAddress').value = document.getElementById('txtAddress').textContent || document.getElementById('txtAddress').value;
        //document.getElementById('txtPerAddress2').value = document.getElementById('txtAddress2').textContent || document.getElementById('txtAddress2').value;
        document.getElementById('txtPerState').value = document.getElementById('txtState').textContent || document.getElementById('txtState').value;
        document.getElementById('txtPerCity').value = document.getElementById('txtCity').textContent || document.getElementById('txtCity').value;
        document.getElementById('txtPerPin').value = document.getElementById('txtPin').textContent || document.getElementById('txtPin').value;
        document.getElementById('txtPerPhone').value = document.getElementById('txtLandline').textContent || document.getElementById('txtLandline').value;
    }
    else {
        document.getElementById('txtPerAddress').value = "";
        //document.getElementById('txtPerAddress2').value = "";
        document.getElementById('txtPerState').value = "";
        document.getElementById('txtPerCity').value = "";
        document.getElementById('txtPerPin').value = "";
        document.getElementById('txtPerPhone').value = "";
    }
    UpdateProgressBar();
}

function getQueryVariable() {    
//   var query = window.location.search.substring(1);
//   //var query = "resumeno=NTQ1NDA4&view=N";//NTQ1NDA2 NTQ0NzYy
//    var vars = query.split("resumeno=");
//    if (vars.length > 1) {
//        var pair = vars[1].split("&view=");        
//        getdata(pair[0], pair[1], "ENCODED");       
//    }

        var resumeNo = document.getElementById('hdnResume').value;
        var view = document.getElementById('hdnView').value;

        if(resumeNo == "" || view == "" || resumeNo == null || view == null)
        {
            alert("Not authorized to view this page");
        }
        else
        {
            getdata(resumeNo, view, "ENCODED");       
        }
}
$(document).ready(function () {
    //document.getElementById("selectCountry").style.display = 'none';
    document.getElementById('divCountry').style.display = 'none';
    getQueryVariable();
});


function NavigateCountryDispaly(calledFrom) {
    if (calledFrom == "OTH") {
        $.ajax({

            type: "GET",
            //url: "http://pwbsqa.wipro.com/cwbs/Services/CWBS_CAM.svc/GetCountyDetails",
            url: "https://synergy.wipro.com/cwbsapp/services/CWBS_CAM.svc/GetCountyDetails",         
            //url: "http://localhost:62349/Services/CWBS_CAM.svc/GetCountyDetails",
            
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (data) {
                var lngt = data.CountryDetails.length;
                $('#ddlCountries').empty();
                $("#ddlCountries").append($("<option></option>").val('Select').html('Select'));
                for (var cnt = 0; eval(cnt) < eval(lngt); cnt++) {
                    $("#ddlCountries").append($("<option></option>").val(data.CountryDetails[cnt].CountryId).html(data.CountryDetails[cnt].CountryDesc));
                }
            },

            error: function OnError(request, result, error) {
                alert("Some Error Occured");
                $('#loadingmessage').hide();
            }
        });
        document.getElementById('divCountry').style.display = 'block';
    }
    else {
        //document.getElementById("selectCountry").style.display = 'none';
         //($("#selectCountry").css("display", "none"));
         document.getElementById('divCountry').style.display = 'none';
    }
    UpdateProgressBar();
}


function PopulateBranch(dropDownId) {
var id = dropDownId.id;
var courseId= $('#'+id+' :selected').text();
    $.ajax({

        type: "GET",
        //url: "http://pwbsqa.wipro.com/cwbs/Services/CWBS_CAM.svc/GetBranchDetails?courseId="+courseId,
        url: "https://synergy.wipro.com/cwbsapp/services/CWBS_CAM.svc/GetBranchDetails?courseId="+courseId,             
        //url: "http://localhost:62349/Services/CWBS_CAM.svc/GetBranchDetails?courseId="+courseId,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (data) {
            
            var rowNum = id.split('_');
            var branchddlId = "SelBranch_" + rowNum[1];
            var lngt = data.BranchDetails.length;
            $('#' + branchddlId + '').empty();
            $("#" + branchddlId + "").append($("<option></option>").val('Select').html('Select'));
            for (var cnt = 0; eval(cnt) < eval(lngt); cnt++) {
                $("#" + branchddlId + "").append($("<option></option>").val(data.BranchDetails[cnt].BRANCH_CODE).html(data.BranchDetails[cnt].BRANCH_DESC));
            }
        },

        error: function OnError(request, result, error) {
            alert("Some Error Occured");
            $('#loadingmessage').hide();
        }
    });
    UpdateProgressBar();
}



function getdata(resumeno, viewtype, restype) {
    if (resumeno != undefined) {
        //  debugger;        
        $('#loadingmessage').show();
        document.getElementById('hdnScroll').value = viewtype;
        var input = {
            "resumeno" : resumeno,
            "isEncoded" : restype
        };
        $.ajax({

            type: "POST",
            
            //url: "http://pwbsqa.wipro.com/CWBS/Services/CWBS_CAM.svc/GetData?resumeno=" + resumeno + "&encoded=" + restype + "",
            //url: "https://confluence.wipro.com/cwbsapp/services/CWBS_CAM.svc/GetData?resumeno=" + resumeno + "&encoded=" + restype + "", 
            //url: "http://localhost:62349/Services/CWBS_CAM.svc/GetData?resumeno=" + resumeno + "&encoded=" + restype + "", 
       
            //url: "http://pwbsqa.wipro.com/CWBS/Services/CWBS_CAM.svc/GetData",
            url: "https://synergy.wipro.com/cwbsapp/services/CWBS_CAM.svc/GetData", 
            //url: "http://localhost:62349/Services/CWBS_CAM.svc/GetData", 
                                                       
            data: JSON.stringify(input),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: true,
            cache: false,
            success: function (data) {
                //  debugger;
                //alert(JSON.stringify(data));
                //data.CountryDetails;
                //                $("#progress-bar").progressbar({ value: 20 });
                //                 $("#progress-bar").css('aria-valuenow','70');                
                
                ($("#lblMsgSuccess").css('display', 'none'));
                ($('#txtIllnessDet').attr("disabled", "disabled"));
                ($('#txtpassportNo').attr("disabled", "disabled"));
                ($('#dtValidtill').attr("disabled", "disabled"));
                ($('#dtInterview').attr("disabled", "disabled"));
                ($('#txtLocation').attr("disabled", "disabled"));
                ($('#txtDivision').attr("disabled", "disabled"));
                ($('#txtRelativeName').attr("disabled", "disabled"));
                ($('#txtRelativeLocation').attr("disabled", "disabled"));
                ($('#txtRelativeDivision').attr("disabled", "disabled"));
                ($('#txtOffense').attr("disabled", "disabled"));
                ($('#txtconviction').attr("disabled", "disabled"));

                //                document.getElementById('lblResumeNo').innerText = "RESUME NUMBER:" + resumeno;
                //                document.getElementById('lblResumeNo').textContent = "RESUME NUMBER:" + resumeno;
                document.getElementById('lblResumeNo').innerHTML = "RESUME NUMBER:" + data.Resumeno;
                document.getElementById('hdnGap').value = data.EduGap;
                if (data.gender == "M") {
                    document.getElementById('chkMale').checked = true;
                }
                else if (data.gender == "F") {
                    document.getElementById('chkFemale').checked = true;
                }
                var lngt = data.CountryDetails.length;
                if (data.nationality != "IND" && data.nationality != "" && data.nationality != null) {
                    document.getElementById('rdOthers').checked = true;
                    document.getElementById('divCountry').style.display = 'block';

                    for (var ct = 0; ct < eval(lngt); ct++) {
                        if (data.CountryDetails[ct].CountryId == data.nationality) {
                            $("#ddlCountries").append($("<option></option>").val(data.CountryDetails[ct].CountryId).html(data.CountryDetails[ct].CountryDesc));
                            // $("#ddlCountries").val(data.CountryDetails[ct].CountryDesc);                          
                            break;
                        }
                    }
                }
                else {
                    document.getElementById('rdIndian').checked = true;
                    document.getElementById('divCountry').style.display = 'none';
                    $("#ddlCountries").append($("<option></option>").val('Select').html('Select'));
                    for (var cnt = 0; eval(cnt) < eval(lngt); cnt++) {
                        $("#ddlCountries").append($("<option></option>").val(data.CountryDetails[cnt].CountryId).html(data.CountryDetails[cnt].CountryDesc));
                    }

                }

                document.getElementById('lblResumeNo').title = resumeno;
                //                document.getElementById('lblEmail1').innerText= data.Emailid;
                //                document.getElementById('lblEmail1').textContent = data.Emailid;
                //document.getElementById('lblEmail1').innerHTML = data.Emailid;
                document.getElementById('lblEmail1').value = data.Emailid;
                if (data.AadhaarNo == null || data.AadhaarNo == "null" || data.AadhaarNo == "NONE") {
                    data.AadhaarNo = "";
                }
                document.getElementById('txtAadhaar').value = data.AadhaarNo;
                if (data.mobileNo == null || data.mobileNo == "null") {
                    data.mobileNo = "";
                }
                document.getElementById('txtMobile').value = data.mobileNo;
                if (data.altmobileNo == null || data.altmobileNo == "null") {
                    data.altmobileNo = "";
                }
                document.getElementById('txtAltMobile').value = data.altmobileNo;
                //document.getElementById('lblFirstName').innerHTML = data.firstname;
                document.getElementById('lblFirstName').value = data.firstname;
                document.getElementById('lblFirstName').title = data.firstname;
                //document.getElementById('lblLastName').innerHTML = data.lastname;
                document.getElementById('lblLastName').value = data.lastname;
                document.getElementById('txtMiddleName').value = data.middlename;
                //document.getElementById('lblBirth').innerHTML = data.dob;
                document.getElementById('lblBirth').value = data.dob;
                //document.getElementById('dtToday').value = data.SubmitDate;
                if (data.Place == null || data.Place == "null") {
                    data.Place = "";
                }
                document.getElementById('txtPlace').value = data.Place;
                if (data.Signature == null || data.Signature == "null") {
                    data.Signature = "";
                }
                document.getElementById('txtSign').value = data.Signature;
//                if (data.SignificantAchivement == null || data.SignificantAchivement == "null") {
//                    data.SignificantAchivement == "";
//                }
//                document.getElementById('txtSignAchive').value = data.SignificantAchivement;
//                if (data.AddInfo == null || data.AddInfo == "null") {
//                    data.AddInfo = "";
//                }
//                document.getElementById('txtAddInfo').value = data.AddInfo;
//                if (data.AreaImprove == null || data.AreaImprove == "null") {
//                    data.AreaImprove = "";
//                }
//                document.getElementById('txtAreasImprove').value = data.AreaImprove;
//                if (data.PreStrengths == null || data.PreStrengths == "null") {
//                    data.PreStrengths = "";
//                }
//                document.getElementById('txtperstrn').value = data.PreStrengths;
//                if (data.OtherInterests == null || data.OtherInterests == "null") {
//                    data.OtherInterests = "";
//                }
//                document.getElementById('txtOtherInterests').value = data.OtherInterests;
//                if (data.CarObj == null || data.CarObj == "null") {
//                    data.CarObj = "";
//                }
//                document.getElementById('txtCarObj').value = data.CarObj;
//                if (data.PreStrengths == null || data.PreStrengths == "null") {
//                    data.PreStrengths = "";
//                }
//                document.getElementById('txtperstrn').value = data.PreStrengths;
//                if (data.Expectations == null || data.Expectations == "null") {
//                    data.Expectations = "";
//                }
//                document.getElementById('txtExpections').value = data.Expectations;
                if (data.Address != null) {
                    if (data.Address.length > 0) {
                        document.getElementById('txtAddress').value = data.Address[0].address;
                        //document.getElementById('txtAddress2').value = data.Address[0].address2;
                        document.getElementById('txtState').value = data.Address[0].state;
                        document.getElementById('txtCity').value = data.Address[0].city;
                        document.getElementById('txtPin').value = data.Address[0].pin;
                        if (data.Address[0].landline == null || data.Address[0].landline == "")
                            data.Address[0].landline = data.mobileNo;
                        document.getElementById('txtLandline').value = data.Address[0].landline;
                        document.getElementById('txtPerAddress').value = data.Address[1].address;
                        //document.getElementById('txtPerAddress2').value = data.Address[1].address2;
                        document.getElementById('txtPerState').value = data.Address[1].state;
                        document.getElementById('txtPerCity').value = data.Address[1].city;
                        document.getElementById('txtPerPin').value = data.Address[1].pin;
                        if (data.Address[1].landline == null || data.Address[1].landline == "")
                            data.Address[1].landline = data.mobileNo;
                        document.getElementById('txtPerPhone').value = data.Address[1].landline;
                    }
                }
                if (data.Education != null) {
                    for (var i = 0; i < data.Education.length; i++) {
                        var x = i + 1;
                        if (data.Education[i].degree != null && data.Education[i].degree != '') {
                            var deg = data.Education[i].degree;
                            if (deg == "BARCH")
                                deg = "B.Arch";
                            else if (deg == "BSC")
                                deg = "B Sc";
                            else if (deg == "BE")
                                deg = "BE / BTech";
                            else if (deg == "BCOM")
                                deg = "B Com";
                            else if (deg == "MTECH")
                                deg = "Full Time M.Tech";
                            else if (deg == "MSC")
                                deg = "M Sc";
                            else if (deg == "PHD")
                                deg = "Phd";
                            else if (deg == "DIP")
                                deg = "Diploma";
                            else if (deg == "ME")
                                deg = "Full Time ME";
                            else if (deg == "INMTECH")
                                deg = "Integrated M.Tech";
                            else if (deg == "INME")
                                deg = "Integrated ME";
                            else if (deg == "INMSC")
                                deg = "Integrated M.Sc";
                            $("#SelCourse_" + x).val(deg);
                        }
                        else
                            $("#SelCourse_" + x).val("Select");

                        //For branch 
                        var lngtBranch = data.BranchDetails.length;
                        $('#SelBranch_' + x).empty();
                        //$('#ddlCountries').empty();
                        if (x < 3) {
                            document.getElementById('txtBranch_' + x).value = data.Education[i].specialization;
                        }
                        else {
                            var branchInfo = "";
                            if (data.Education[i].specialization != "" && data.Education[i].specialization != null && data.Education[i].specialization != "Select") {
                                for (var cnt = 0; eval(cnt) < eval(lngtBranch); cnt++) {
                                    if (data.BranchDetails[cnt].BRANCH_CODE == data.Education[i].specialization) {
                                        branchInfo = "S";
                                        $("#SelBranch_" + x).append($("<option></option>").val(data.BranchDetails[cnt].BRANCH_CODE).html(data.BranchDetails[cnt].BRANCH_DESC));
                                        break;
                                    }
                                }
                                if (branchInfo != "S") {//For old cases
                                    $("#SelBranch_" + x).append($("<option></option>").val(data.Education[i].specialization).html(data.Education[i].specialization));
                                }
                            }
                            else {
                                $("#SelBranch_" + x).append($("<option></option>").val('Select').html('Select'));
                                for (var cnt = 0; eval(cnt) < eval(lngtBranch); cnt++) {
                                    $("#SelBranch_" + x).append($("<option></option>").val(data.BranchDetails[cnt].BRANCH_CODE).html(data.BranchDetails[cnt].BRANCH_DESC));
                                }
                            }
                        }

                        //University
                        document.getElementById('txtUniversity_' + x).value = data.Education[i].university;
                        document.getElementById('txtNameUni_' + x).value = data.Education[i].uniName;
                        document.getElementById('txtlocation_' + x).value = data.Education[i].location;


                        //                        if (data.Education[i].month != null && data.Education[i].month != "")
                        //                            document.getElementById('dtToGrad_' + x).value = data.Education[i].month + "/";
                        //                        document.getElementById('dtToGrad_' + x).value = document.getElementById('dtToGrad_' + x).value + data.Education[i].year;
                        document.getElementById('dtToGrad_' + x).value = data.Education[i].toDate;
                        document.getElementById('dtFromGrad_' + x).value = data.Education[i].fromDate;

                        document.getElementById('txtAggregate_' + x).value = data.Education[i].aggregate;
                        ////////                        if (data.Action == "SUBMITED") {
                        ////////                            $("#dtToGrad_" + x).datepicker().datepicker('disable');

                        ////////                        }
                        ////// ($('#dtGrad_' + x).removeAttr("disabled"));
                    }
                }

                //Delete rows from gap table
                 var tblGapLength = document.getElementById("tblgapDetails").rows.length; 
                 if (tblGapLength > 1) {
                     for(var lnGp=2;lnGp<=tblGapLength;lnGp++){
                            $('#tblgapDetails tr:last-child').remove();
                      }
                 }
                if(data.Education[0].gapReason!="" ||  data.Education[1].gapReason!="" || data.Education[2].gapReason!=""){
                       if(data.Education[0].gapReason!="" && data.Education[0].gapReason){
                             AddGapTableRows(1,"getdata",data);
                       }
                       if(data.Education[1].gapReason!="" && data.Education[1].gapReason!=null){
                            AddGapTableRows(2,"getdata",data);
                       }
                       if(data.Education[2].gapReason!="" && data.Education[2].gapReason){
                            AddGapTableRows(3,"getdata",data);
                       }
                }              

                if (data.PrevEmp != null) {
                    for (var i = 0; i < data.PrevEmp.length; i++) {
                        removePrevEmpRows();
                    }
                    for (var i = 0; i < data.PrevEmp.length; i++) {
                        var x = i + 1;
                        addPrevEmpRows();
                        document.getElementById('txtPrevOrgName_' + x).value = data.PrevEmp[i].prevOrgName;
                        document.getElementById('txtPrevlocation_' + x).value = data.PrevEmp[i].prevLocation;
                        document.getElementById('txtPrevDesig_' + x).value = data.PrevEmp[i].prevDesig;
                        document.getElementById('txtPrevEmpCode_' + x).value = data.PrevEmp[i].prevEmpCode;
                        document.getElementById('txtPrevFromDt_' + x).value = data.PrevEmp[i].prevFromDt;
                        document.getElementById('txtPrevToDt_' + x).value = data.PrevEmp[i].prevToDt;
                        document.getElementById('txtPrevStarting_' + x).value = data.PrevEmp[i].prevStSal;
                        document.getElementById('txtPrevFinal_' + x).value = data.PrevEmp[i].prevEndSal;
                        document.getElementById('txtPrevLeaveReason_' + x).value = data.PrevEmp[i].prevLeaveRsn;
                        if (data.Action == "SUBMITED") {
                            $("#txtPrevFromDt_" + x).datepicker().datepicker('disable');
                            $("#txtPrevToDt_" + x).datepicker().datepicker('disable');
                        }
                        ($("#txtPrevFromDt_" + x).removeAttr("disabled"));
                        ($("#txtPrevToDt_" + x).removeAttr("disabled"));
                    }
                }
                if (data.Training != null) {
                    for (var i = 0; i < data.Training.length; i++) {
                        removeTrainingRows();
                    }
                    for (var i = 0; i < data.Training.length; i++) {
                        var x = i + 1;
                        addTrainingRows();
                        document.getElementById('txtTrainOrg_' + x).value = data.Training[i].trainOrg;
                        document.getElementById('txtSkills_' + x).value = data.Training[i].trainSkills;
                        document.getElementById('txtTrainLocation_' + x).value = data.Training[i].trainLocation;
                        document.getElementById('txtTrainFromDt_' + x).value = data.Training[i].trainStartDt;
                        document.getElementById('txtTrainToDt_' + x).value = data.Training[i].trainEndDt;
//                        if (data.Action == "SUBMITED") {
//                            $("#txtTrainFromDt_" + x).datepicker().datepicker('disable');
//                            $("#txtTrainToDt_" + x).datepicker().datepicker('disable');
//                        }
                    }
                }

                if (data.Project != null) {
                    for (var i = 0; i < data.Project.length; i++) {
                        removeProjectRows();
                    }
                    for (var i = 0; i < data.Project.length; i++) {
                        var x = i + 1;
                        addProjectRows();
                        document.getElementById('txtProjName_' + x).value = data.Project[i].projName;
                        document.getElementById('txtProjSkills_' + x).value = data.Project[i].projSkills;
                        document.getElementById('txtProjDesc_' + x).value = data.Project[i].projDescription;
                        document.getElementById('txtProjLocation_' + x).value = data.Project[i].projLocation;
                        document.getElementById('txtProjFromDt_' + x).value = data.Project[i].projStartDt;
                        document.getElementById('txtProjToDt_' + x).value = data.Project[i].projEndDt;
                    }
                }
                if (data.maths ==  "Y")
                    document.getElementById('rdMathsYes').checked = true;
                else if (data.maths ==  "N")
                    document.getElementById('rdMathsNo').checked = true;

                if (data.EduDeclaration == "Y")
                    document.getElementById('rdEduDecYes').checked = true;
                else if (data.EduDeclaration == "N")
                    document.getElementById('rdEduDecNo').checked = true;

                if (data.Reference != null) {
                    for (var i = 0; i < data.Reference.length; i++) {
                        removetblRefRows();
                    }
                    for (var i = 0; i < data.Reference.length; i++) {
                        var x = i + 1;
                        addtblRefRows();
                        document.getElementById('txtRefName_' + x).value = data.Reference[i].refname;
                        document.getElementById('txtAssociation_' + x).value = data.Reference[i].association;
                        document.getElementById('txtOrgDesig_' + x).value = data.Reference[i].orgDesig;
                        document.getElementById('txtEmail_' + x).value = data.Reference[i].email;
                        document.getElementById('txtPhone_' + x).value = data.Reference[i].phone;
                        document.getElementById('txtResidence_' + x).value = data.Reference[i].residence;
                    }
                }
                if (data.PassPort != null) {
                    if (data.PassPort.length > 0) {
                        if (data.PassPort[0].ValidPass == "Y") {
                            document.getElementById('rdPassYes').checked = true;
                            document.getElementById('txtpassportNo').value = data.PassPort[0].PassPortNo;
                            document.getElementById('dtValidtill').value = data.PassPort[0].Validtill;
                            ($('#txtpassportNo').removeAttr("disabled"));
                            ($('#dtValidtill').removeAttr("disabled"));
                            //if (data.Action == "SUBMITED")
                            //  $("#dtValidtill").datepicker().datepicker('disable'); 
                        }
                        else {
                            document.getElementById('rdPassNo').checked = true;
                            ($('#txtpassportNo').attr("disabled", "disabled"));
                            ($('#dtValidtill').attr("disabled", "disabled"));
                        }
                    }
                }
                else if (data.PassPort == null) {
                    document.getElementById('rdPassNo').checked = true;
                    ($('#txtpassportNo').attr("disabled", "disabled"));
                    ($('#dtValidtill').attr("disabled", "disabled"));
                }
                if (data.WiproApplied != null) {
                    if (data.WiproApplied.length > 0) {
                        if (data.WiproApplied[0].ValidApply == "Y") {
                            document.getElementById('rdApplyYes').checked = true;
                            document.getElementById('dtInterview').value = data.WiproApplied[0].InterviewDate;
                            document.getElementById('txtLocation').value = data.WiproApplied[0].InterviewLocation;
                            document.getElementById('txtDivision').value = data.WiproApplied[0].InterviewDivision;
                            ($('#dtInterview').removeAttr("disabled"));
                            if (data.Action == "SUBMITED")
                                $("#dtInterview").datepicker().datepicker('disable');
                            ($('#txtLocation').removeAttr("disabled"));
                            ($('#txtDivision').removeAttr("disabled"));
                        }
                        else {
                            document.getElementById('rdApplyNo').checked = true;
                            ($('#dtInterview').attr("disabled", "disabled"));
                            ($('#txtLocation').attr("disabled", "disabled"));
                            ($('#txtDivision').attr("disabled", "disabled"));
                        }
                    }
                }
                else if (data.WiproApplied == null) {
                    document.getElementById('rdApplyNo').checked = true;
                    ($('#dtInterview').attr("disabled", "disabled"));
                    ($('#txtLocation').attr("disabled", "disabled"));
                    ($('#txtDivision').attr("disabled", "disabled"));
                }
                if (data.Illness != null) {
                    if (data.Illness.length > 0) {
                        if (data.Illness[0].Ill == "Y") {
                            document.getElementById('rdIllnessYes').checked = true;
                            document.getElementById('txtIllnessDet').value = data.Illness[0].IllnessDet;
                            ($('#txtIllnessDet').removeAttr("disabled"));
                        }
                        else {
                            document.getElementById('rdIllnessNo').checked = true;
                            ($('#txtIllnessDet').attr("disabled", "disabled"));
                        }
                    }
                }
                else if (data.Illness == null) {
                    document.getElementById('rdIllnessNo').checked = true;
                    ($('#txtIllnessDet').attr("disabled", "disabled"));
                }
                if (data.RelativeInWipro != null) {
                    if (data.RelativeInWipro.length > 0) {
                        if (data.RelativeInWipro[0].Relative == "Y") {
                            document.getElementById('txtRelativeName').value = data.RelativeInWipro[0].RelativeName;
                            document.getElementById('txtRelativeLocation').value = data.RelativeInWipro[0].RelativeLocation;
                            document.getElementById('txtRelativeDivision').value = data.RelativeInWipro[0].RelativeDivision;
                            document.getElementById('rdRelativeYes').checked = true;
                            ($('#txtRelativeName').removeAttr("disabled"));
                            ($('#txtRelativeLocation').removeAttr("disabled"));
                            ($('#txtRelativeDivision').removeAttr("disabled"));
                        }
                        else {
                            document.getElementById('rdRelativeNo').checked = true;
                            ($('#txtRelativeName').attr("disabled", "disabled"));
                            ($('#txtRelativeLocation').attr("disabled", "disabled"));
                            ($('#txtRelativeDivision').attr("disabled", "disabled"));
                        }
                    }
                }
                else if (data.RelativeInWipro == null) {
                    document.getElementById('rdRelativeNo').checked = true;
                    ($('#txtRelativeName').attr("disabled", "disabled"));
                    ($('#txtRelativeLocation').attr("disabled", "disabled"));
                    ($('#txtRelativeDivision').attr("disabled", "disabled"));
                }
                if (data.Arrested != null) {
                    if (data.Arrested.length > 0) {
                        if (data.Arrested[0].Arrest == "Y") {
                            document.getElementById('txtconviction').value = data.Arrested[0].Conviction;
                            document.getElementById('txtOffense').value = data.Arrested[0].Offense;
                            document.getElementById('rdArrestedYes').checked = true;
                            ($('#txtOffense').removeAttr("disabled"));
                            ($('#txtconviction').removeAttr("disabled"));
                        }
                        else {
                            document.getElementById('rdArrestedNo').checked = true;
                            ($('#txtOffense').attr("disabled", "disabled"));
                            ($('#txtconviction').attr("disabled", "disabled"));
                        }
                    }
                }
                else if (data.Arrested == null) {
                    document.getElementById('rdArrestedNo').checked = true;
                    ($('#txtOffense').attr("disabled", "disabled"));
                    ($('#txtconviction').attr("disabled", "disabled"));
                }
                if (data.Locations != null) {
                    if (data.Locations.length > 0) {
                        if (data.Locations[0].Location1 != null && data.Locations[0].Location1 != "")
                            $("#selLocation_1").val(data.Locations[0].Location1);
                        else
                            $("#selLocation_1").val("Select");
                        if (data.Locations[0].Location2 != null && data.Locations[0].Location2 != "")
                            $("#selLocation_2").val(data.Locations[0].Location2);
                        else
                            $("#selLocation_2").val("Select");
                        if (data.Locations[0].Location3 != null && data.Locations[0].Location3 != "")
                            $("#selLocation_3").val(data.Locations[0].Location3);
                        else
                            $("#selLocation_3").val("Select");
                    }
                }
                else if (data.Locations == null) {
                    $("#selLocation_1").val("Select");
                    $("#selLocation_2").val("Select");
                    $("#selLocation_3").val("Select");
                }
                //debugger;
                $("#dtToday").datepicker({ dateFormat: "dd-M-yy" });
                $("#dtToday").datepicker('setDate', new Date());
                ($('#dtToday').attr("disabled", "disabled"));

                ($("#txtUniversity_1").attr('disabled', 'disabled'));
                ($("#txtUniversity_2").attr('disabled', 'disabled'));
                ($("#txtUniversity_4").attr('disabled', 'disabled'));
//                ($("#txtUniversity_3").removeAttr("disabled"));
//                ($("#txtUniversity_3").removeAttr("readonly"));


                if (data.Action == "SAVED"){                
                if (data.Education[3].degree != "" && data.Education[3].degree != null && data.Education[3].degree != "Select") {
                        ($("#dtToGrad_4").removeAttr("disabled"));
                        ($("#dtFromGrad_4").removeAttr("disabled"));
                        ($("#txtAggregate_4").removeAttr("disabled"));                        
                        ($("#SelBranch_4").removeAttr("disabled"));
                        ($("#txtUniversity_4").removeAttr("disabled"));
                        ($("#txtNameUni_4").removeAttr("disabled"));
                        ($("#txtlocation_4").removeAttr("disabled"));
                    }
                    else{
                        ($("#dtToGrad_4").attr('disabled', 'disabled'));
                        ($("#dtFromGrad_4").attr('disabled', 'disabled'));
                        ($("#txtAggregate_4").attr('disabled', 'disabled'));
                        ($("#SelBranch_4").attr('disabled', 'disabled'));                        
                        ($("#txtNameUni_4").attr('disabled', 'disabled'));
                        ($("#txtlocation_4").attr('disabled', 'disabled'));                    
                    }
                }
                if (data.Action == "SUBMITED") {
                    //                    $('#form1').find('input, textarea, button, select').attr('disabled', 'disabled');
                    $("#form1 :input").attr("readonly", true);
                    $("input[type=button]").attr("disabled", "disabled");
                    $("input[type=radio]").attr("disabled", "disabled");                    
                    $('#form1').find('select').attr('disabled', 'disabled');
                    ($("#btnCollapse").removeAttr("disabled"));
                    ($("#btnExpand").removeAttr("disabled"));
                    ($("#btnTrngAdd").removeAttr("disabled"));
                    ($("#btnTrngRemove").removeAttr("disabled"));
                    ($("#btnProjectAdd").removeAttr("disabled"));
                    ($("#btnProjectRemove").removeAttr("disabled"));
                    ($("#txtAddress").removeAttr("readonly"));
                    //($("#txtAddress2").removeAttr("readonly"));
                    ($("#txtState").removeAttr("readonly"));
                    ($("#txtCity").removeAttr("readonly"));
                    ($("#txtPin").removeAttr("readonly"));
                    ($("#txtLandline").removeAttr("readonly"));
                    ($("#txtPerAddress").removeAttr("readonly"));
                    //($("#txtPerAddress2").removeAttr("readonly"));
                    ($("#txtPerState").removeAttr("readonly"));
                    ($("#txtPerCity").removeAttr("readonly"));
                    ($("#txtPerPin").removeAttr("readonly"));
                    ($("#txtPerPhone").removeAttr("readonly"));
                    ($("#btnSubmit").removeAttr("disabled"));
                    ($("#btnSave").attr('disabled', 'disabled'));                    
                    ($("#txtMobile").removeAttr("readonly"));
                    ($("#txtAltMobile").removeAttr("readonly"));
                    ($("#txtAadhaar").removeAttr("readonly"));
                    ($("#txtpassportNo").removeAttr("readonly"));       
                     if(data.PassPort[0].PassPortNo!="" && data.PassPort[0].PassPortNo!=null){
                        ($("#dtValidtill").removeAttr("disabled"));
                        ($("#txtpassportNo").removeAttr("disabled"));
                    }                                                     
                    ($("#rdPassYes").removeAttr("disabled"));   
                    ($("#rdPassNo").removeAttr("disabled"));   
                    ($("#selLocation_1").removeAttr("disabled"));
                    ($("#selLocation_2").removeAttr("disabled"));
                    ($("#selLocation_3").removeAttr("disabled"));                    
                    ($("#selLocation_3").removeAttr("disabled"));
                    ($("#txtBranch_1").attr('disabled', 'disabled'));
                    ($("#txtBranch_2").attr('disabled', 'disabled'));  
                    ($("#txtUniversity_3").removeAttr("disabled"));
                    ($("#txtUniversity_3").removeAttr("readonly"));                
                    if (data.Education[3].degree == "" || data.Education[3].degree == null || data.Education[3].degree == "Select") {
                        ($("#dtToGrad_3").removeAttr("disabled"));
                        ($("#dtFromGrad_3").removeAttr("disabled"));
                        ($("#txtAggregate_3").removeAttr("disabled"));
                        ($("#txtAggregate_3").removeAttr("readonly"));                       
                    }
                    else {
                        ($("#dtToGrad_4").removeAttr("disabled"));
                        ($("#dtFromGrad_4").removeAttr("disabled"));
                        ($("#txtAggregate_4").removeAttr("disabled"));
                        ($("#txtAggregate_4").removeAttr("readonly"));
                        ($("#txtUniversity_4").removeAttr("disabled"));
                        ($("#txtUniversity_4").removeAttr("readonly"));
                    }                

                    $("#dtValidtill").datepicker({ dateFormat: "dd-M-yy", changeMonth: true,
                        changeYear: true,
                        showButtonPanel: true,
                        beforeShow: function (el, dp) {
                            $('#ui-datepicker-div')[$(el).is('[data-calendar="false"]') ? 'removeClass' : 'addClass']('hide-calendar');
                        }
                    });

                    //                    if (data.Education[3].degree != null && data.Education[3].degree != "") {
                    //                        ($("#txtAggregate_4").removeAttr("readonly"));
                    //                        $("#dtToGrad_4").datepicker({
                    //                            changeMonth: true,
                    //                            changeYear: true,
                    //                            showButtonPanel: true,
                    //                            dateFormat: 'M/yy',

                    //                            onClose: function () {
                    //                                var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    //                                var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    //                                $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
                    //                            },

                    //                            beforeShow: function (el, dp) {
                    //                                $('#ui-datepicker-div')[$(el).is('[data-calendar="false"]') ? 'addClass' : 'removeClass']('hide-calendar');
                    //                            }
                    //                        });
                    //                    }
                    //                    else {
                    //                        ($("#dtGrad_3").removeAttr("readonly"));
                    //                        ($("#txtAggregate_3").removeAttr("readonly"));
                    //                    }

                    document.getElementById('hdnScroll').value = viewtype;

                    if (data.Training != null) {
                        for (var i = 0; i < data.Training.length; i++) {
                            var x = i + 1;
                            document.getElementById('txtTrainOrg_' + x).removeAttribute("readonly");
                            document.getElementById('txtSkills_' + x).removeAttribute("readonly");
                            document.getElementById('txtTrainLocation_' + x).removeAttribute("readonly");
                        }
                    }

                    if(data.Project != null) {
                        for (var i = 0; i < data.Project.length; i++) {
                            var x = i + 1;
                            document.getElementById('txtProjName_' + x).removeAttribute("readonly");
                            document.getElementById('txtProjSkills_' + x).removeAttribute("readonly");
                            document.getElementById('txtProjDesc_' + x).removeAttribute("readonly");
                            document.getElementById('txtProjLocation_' + x).removeAttribute("readonly");
                        }
                    }
                    document.getElementById('lblEmail1').removeAttribute("readonly");
                }
                if (viewtype == "Y" && data.Action == "SUBMITED") {
                    $("input[type=button]").attr("disabled", "disabled");                    
                    $("#form1 :input").attr("readonly", true);
                    $('#form1').find('select').attr('disabled', 'disabled');
                    document.getElementById('hdnScroll').value = viewtype;
                    ($("#btnSubmit").css('display', 'none'));
                    ($("#btnCollapse").css('display', 'none'));
                    ($("#btnExpand").css('display', 'none'));
                    ($("#btnSave").css('display', 'none'));
                    ($("#btnempadd").css('display', 'none'));
                    ($("#btnempdel").css('display', 'none'));
                    ($("#btnRefAdd").css('display', 'none'));
                    ($("#ButRefRem").css('display', 'none'));
                    ($("#btnTrngRemove").css('display', 'none'));
                    ($("#btnTrngAdd").css('display', 'none'));
                    $("#divPer").addClass("active").next().slideDown();
                    $("#divPer").css('background', '#E0E0E0');
                    $("#divEdu").addClass("active").next().slideDown();
                    $("#divEdu").css('background', '#E0E0E0');
                    $("#divRef").addClass("active").next().slideDown();
                    $("#divRef").css('background', '#E0E0E0');
                    $("#divEmp").addClass("active").next().slideDown();
                    $("#divEmp").css('background', '#E0E0E0');
                    $("#divAdd").addClass("active").next().slideDown();
                    $("#divAdd").css('background', '#E0E0E0');
                    $("#divDec").addClass("active").next().slideDown();
                    $("#divDec").css('background', '#E0E0E0');


                } else if (viewtype == "Y" && data.Action == "SAVED") {
                    $('#form1').css('display', 'none');
                    alert("CAM form has not been submitted for this resume number");
                }
                else if (data.Action != "SAVED" && data.Action != "SUBMITTED" && data.Action != "" && data.Action != null) {
                }

                $('#loadingmessage').hide();
                UpdateProgressBar();
            },

            error: function OnError(request, result, error) {
                alert("Some Error Occured");
                $('#loadingmessage').hide();
            }
        });
    }
}
