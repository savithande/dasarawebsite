/*-----------------------------------------------------------------------------------*/
/* 		Main Js Start 
/*-----------------------------------------------------------------------------------*/
$(document).ready(function($) {
	"use strict"
	 $(".venues").hide();
	 $(".city").hide();
	 $(".ven").show();
	 $(".ven").click(function(){
        $(".cities").hide();
		$(".venues").show();
		 $(".ven").hide();
		 $(".city").show();
    });
	$(".city").click(function(){
        $(".cities").show();
		$(".venues").hide();
		 $(".ven").show();
		 $(".city").hide();
    });
	  
	$(".client-slide").owlCarousel({ 
		autoplay:true,
		autoplayHoverPause:true,
		singleItem	: true,
		navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
		lazyLoad:true,
		nav: false,
		loop:true,
		margin:30,
		responsive:{
			0:{
				items:1
			},
			675:{
				items:2
			},
			1200:{
				items:3
			}}	
	});	
	/*-----------------------------------------------------------------------------------*/
	/* Pretty Photo
	/*-----------------------------------------------------------------------------------*/
	jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
		theme: "light_square"
	});
	/*-----------------------------------------------------------------------------------*/
	/* 		Parallax
	/*-----------------------------------------------------------------------------------*/
	jQuery.stellar({
	   horizontalScrolling: false,
	   scrollProperty: 'scroll',
	   positionProperty: 'position'
	});
	smoothScroll.init();
	
	$(".kannada").hide();
	 $(".en").hide();
	 $(".ka").show();
	 $(".ka").click(function(){
        $(".english").hide();
		$(".kannada").show();
		 $(".ka").hide();
		 $(".en").show();
    });
	$(".en").click(function(){
        $(".english").show();
		$(".kannada").hide();
		 $(".ka").show();
		 $(".en").hide();
    });
	 
	
	
});
